<?php

use Carbon\Carbon;
use Faker\Factory as Faker;

class RegisteruserCest
{
    /**
     * Faker\Factory
     * 
     * @var $fake
     */
    protected $fake; 

    /**
     * Create new Faker
     * 
     */
    public function __construct()
    {
        $this->fake = Faker::create();
    }

    public function _before(FunctionalTester $I)
    {       
        $I->amOnPage('register');
        $I->wantTo('register new User');
    }

    public function _after(FunctionalTester $I)
    {
    }

    // tests
    // public function it_creates_new_agent(FunctionalTester $I)
    // {
    //     $I->submitForm('.registerForm',[
    //         'firstname' => $this->fake->firstname,
    //         'middlename' => $this->fake->lastname,
    //         'lastname'  => $this->fake->lastname,
    //         'email' => $this->fake->safeEmail,
    //         'password' => 'password',
    //         'password_confirmation' => 'password'
    //         'usertype' => 'App\UserAgent'
    //     ]);

    //     $I->see('logout');

    // }

    public function it_creates_new_customer(FunctionalTester $I)
    {
        $I->submitForm('.registerForm',[
            'firstname' => $this->fake->firstname,
            'middlename' => $this->fake->lastname,
            'lastname'  => $this->fake->lastname,
            'email' => $this->fake->safeEmail,
            'password' => 'password',
            'password_confirmation' => 'password',
            'usertype' => 'App\UserCustomer' 
        ]);

        $I->see('logout');

    }

    public function it_displays_required_fields(FunctionalTester $I)
    {
        $I->submitForm('.registerForm',[
            'firstname' => '',
            'middlename' => '',
            'lastname'  => '',
            'email' => '',
            'password' => 'password',
            'password_confirmation' => 'password',
            'registerAs' => false,
            'dynamic' => '',
            'address' => '',
            'usertype' => 'App\UserCustomer' ?: 'App\UserAgent'
        ]);

        $I->see('the email field is required');
    }

    public function it_requires_required_password(FunctionalTester $I)
    {
        $I->submitForm('.registerForm',[
            'firstname' => $this->fake->firstname,
            'middlename' => $this->fake->lastname,
            'lastname'  => $this->fake->lastname,
            'email' => $this->fake->safeEmail,
            'password' => 'password',
            'password_confirmation' => '123456',
            'registerAs' => false,
            'dynamic' => $this->fake->company,
            'address' => $this->fake->address,
            'usertype' => 'App\UserCustomer' ?: 'App\UserAgent'
        ]);

        $I->see('The password confirmation does not match.');
    }

    public function it_displays_duplicate_email(FunctionalTester $I)
    {
        $I->submitForm('.registerForm',[
            'firstname' => $this->fake->firstname,
            'middlename' => $this->fake->lastname,
            'lastname'  => $this->fake->lastname,
            'email' => 'customer@example.com',
            'password' => 'password',
            'password_confirmation' => '123456',
            'registerAs' => false,
            'dynamic' => $this->fake->company,
            'address' => $this->fake->address,
            'usertype' => 'App\UserCustomer'
        ]);

        $I->see('The email has already been taken.');
    }
}
