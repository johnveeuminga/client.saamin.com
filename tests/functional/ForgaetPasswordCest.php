<?php


class ForgaetPasswordCest
{

    public function _before(FunctionalTester $I)
    {
        $I->wantTo('reset password');
    }

    public function _after(FunctionalTester $I)
    {

    }

    // tests
    public function tryToTest(FunctionalTester $I)
    {
        $I->amOnPage('password/reset');
        $I->see('Send Password Reset Link');

        $I->fillField('email','agent404@example.com');
        $I->click('Send Password Reset Link');

        $I->see('We have e-mailed your password reset link!');

    }
}
