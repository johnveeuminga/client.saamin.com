<?php

use Faker\Factory as Faker;

class PropertyCest
{
    /**
     * Faker\Factory
     * 
     * @var $fake
     */
    protected $faker; 

    /**
     * Create new Faker
     * 
     */
    public function __construct()
    {
        $this->faker = Faker::create();
    }

    public function _before(FunctionalTester $I)
    {

    }

    public function _after(FunctionalTester $I)
    {
    }

    public function it_displays_all_properties(FunctionalTester $I)
    {
        $I->amOnPage('/properties');
        $I->wantTo('view all Properties');

        $I->seeElement('#results');
        $I->see('bedroom');
        $I->see('Php');
    }

    public function it_displays_single_properties(FunctionalTester $I)
    {

        $I->amOnPage('/properties/1');
        $I->wantTo('view single property');

        $I->seeElement('#map');
        $I->seeElement('video');
    }

    public function it_create_a_property(FunctionalTester $I)
    {
        $I->login('agent404@example.com', 'password');

        $I->amOnPage('/properties/create');
        $I->wantTo('create a single property');

        $I->submitForm('.propertyform',[

            'name' => $this->faker->secondaryAddress,
            'address' => $this->faker->address,
            'size' =>$this->faker->randomFloat($nbMaxDecimals = 5, $min=0, $max=500000),
            'user_agent_id' => $this->faker->numberBetween($min = 1, $max = 6),
            'description' => $this->faker->paragraph($nb=1, $asText=true),
            'lat' => $this->faker->latitude($nbMaxDecimals = 5, $min=1000, $max=1500),
            'long' => $this->faker->longitude($nbMaxDecimals= 5, $min=1000, $max=1500),
            'price' => $this->faker->numberBetween($min=400000, $max=1500000),
            'bedrooms' =>$this->faker->numberBetween($min=2, $max=5),
            'bathrooms' => $this->faker->numberBetween($min=2, $max=5)

        ]);

        $I->seeInCurrentUrl('/properties');

    }

    public function it_updates_a_single_property(FunctionalTester $I)
    {
        $I->login('agent404@example.com', 'password');

        $I->amOnPage('/properties/1/edit');
        $I->wantTo('Update a single property');

        $I->submitForm('.propertyformedit',[

            'name' => $this->faker->secondaryAddress,
            'address' => $this->faker->address,
            'size' =>$this->faker->randomFloat($nbMaxDecimals = 5, $min=0, $max=500000),
            'user_agent_id' => $this->faker->numberBetween($min = 1, $max = 6),
            'description' => $this->faker->paragraph($nb=1, $asText=true),
            'lat' => $this->faker->latitude($nbMaxDecimals = 5, $min=1000, $max=1500),
            'long' => $this->faker->longitude($nbMaxDecimals= 5, $min=1000, $max=1500),
            'price' => $this->faker->numberBetween($min=400000, $max=1500000),
            'bedrooms' =>$this->faker->numberBetween($min=2, $max=5),
            'bathrooms' => $this->faker->numberBetween($min=2, $max=5)

        ]);

        // $I->see('Property succesfully updated.');
    }
}
