<?php

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;

class RegisterControllerTest extends TestCase
{
	protected $client;

	public function setUp()
	{
		parent::setUp();

		$this->client = new Client;

	}

    /** @test */
    public function it_get_access_token_from_api_using_client_credentials()
    {
        $response = $this->client->post('http://saamin.com/oauth/token',[

			'form_params' => [

				'client_id'		=> env('CLIENT_ID'),
				'client_secret' => env('CLIENT_SECRET'),
				'grant_type' 	=> 'client_credentials',
				'scope' 		=> '*'
			]
		]);

		$token = json_decode($response->getBody(), true);

		$this->assertEquals(200, $response->getStatusCode());

		$this->assertArrayHasKey('access_token', $token);

    }

}
