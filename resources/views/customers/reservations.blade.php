@extends('layouts.master')
    
@section('content')
    <div class="container">
    	@include('components.success')
 	
		<br>
		<!-- card panel for properties -->
	  	<div class="card ">
	  		<div class="card-header">
				Booked Properties
			</div>
			@foreach($properties as $property)
		  		<div class="card-block">
		  			<ul class="list-group">
			  			<li class="list-group-item">
			  				<h4 class="card-title">{{ $property->name }}</h4>
			  				<p class="card-text">{{ $property->description }}</p>
		      				<p class="card-text"><small class="text-muted">
		      					Last updated {{ $property->human_time }}</small>
		      				</p>
		      				<img class="card-img-top img-fluid" src="{{ $property->cover_image }}" alt="House Image">
		      				<hr>
		      				<a href='{{ url("/properties/{$property->id}") }}' class="btn btn-danger">Visit Page</a>
			  			</li>
		  			</ul>
		  		</div>
	  		@endforeach
	  	</div>
		<!-- end  -->    	
  
    </div>
@endsection