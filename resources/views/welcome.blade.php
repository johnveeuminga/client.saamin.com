@extends('layouts.master')
@section('title')
   <title>{{ config('app.name', 'Saamin.com - A property search website') }}</title>
@endsection
@section('content')
<section>
  @include('components.success')
  <div class="jumbotron row">
    <div class="col">
      <h1 class="text-center">A JOURNEY TO YOUR NEXT HOME!</span></h1>
      <p class="text-center">SAAMIN.COM HELPS YOU FIND YOUR HOME!</p>
      <hr class="m-y-2">
      <div class="search-form fadeInUp">
        <div class="aa-input-container" id="aa-input-container">
          <input type="search" id="aa-search-input" class="aa-input-search form-control" placeholder="Search for your location..." name="search" autocomplete="off" />
         
          <div class="button-holder">
            <button type="submit" class="btn btn-warning btn-lg">SEARCH</button>
          </div>
        </div>
      </div>
    </div>
    <div class="col">
      <div id="map"></div>
    </div>
  </div>
</section>

<section class="container mt-5 mb-5 pb-5 pt-5">
  <h1 class="h3 text-center">Featured Properties</h1>
  <hr>
  
  <div class="card-group">
  @foreach($props as $prop)
    <a href="{{ route('properties.show', $prop->slug ) }}" class="card text-danger">
      <div class="card-header bg-warning">
        <h1 class="h4 text-center text-white">{{ $prop->name }}</h1>
      </div>

      <img class="card-img-top" src="{{ $prop->image_thumbnail }}">

      <div class="card-block">
        <p class="card-text">{{ $prop->address }}</p>
        <p class="card-text">{{ $prop->description }}</p>
      </div>

      <div class="card-footer">
        <small class="text-muted">Created {{ $prop->created_at }}</small>
      </div>
    </a>
  @endforeach
  </div>

</section>

<section class="pb-5 pt-5 bg-faded">
  <div class="container mt-5 mb-5">
    <h1 class="h3 text-center">Frequently Answered Questions</h1>
    <hr>
    
    <div class="card-group">
      <div class=card>
        <div class="card-block">
          <h1 class="h4">What does this website do?</h1>
          <p class="card-text">Saamin is an online marketplace and hospitality service, enabling people to list or rent short-term lodging including vacation rentals, apartment rentals, homestays, hostel beds, or hotel rooms, with the cost of lodging set by the property owner. The company receives percentage service fees from both guests and hosts in conjunction with every booking.</p>
        </div>
      </div>
      <div class=card>
        <div class="card-block">
          <h1 class="h4">Do I have to pay to sign up?</h1>
          <p class="card-text">No, this website is free when you sign up for an account.</p>
        </div>
      </div>
      <div class=card>
        <div class="card-block">
          <h1 class="h4">How to get started?</h1>
          <p class="card-text">Create an account then login, this will redirect you to your dashboard where you can now start looking for a place to stay in</p>
        </div>
      </div>

    </div>
  </div>
</section>


<section class="bg-warning">
  <div class="container">
    <div class="row text-muted">
      <div class="col bg-faded pt-4 pb-4">
        <h1 class="h4 text-center">Feel free to contact us</h1>
        <hr>
        <form class="col rounded contactForm" method="POST" action="{{ url('/contactUs') }}">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="Name">Name</label>
                <input type="text" name="name" class="form-control" placeholder="Name" id="Name">
            </div>
          <div class="form-group">
            <label for="email">Email address</label>
            <input type="email" name="email" required class="form-control" placeholder="Enter email" id="email">
            <small class="form-text text-muted">We'll never share your email with anyone else.</small>
          </div>
          <div class="form-group">
              <label for="msg"> Message </label>
              <textarea name="message" required id="msg" class="form-control" placeholder="Message"></textarea>
          </div>
          <div class="text-center">
            <button type="submit" class="btn btn-info btnContact" id="btnContactUs">Send Message</button>
          </div>
        </form>
        <div class="row">
          <div class="col text-center mt-5">
            <h1 class="h4">Our office</h1>
            <hr>
            <address>
                <strong>Four K Software</strong><br>
                189 Holyghost Extension<br>
                Purok 4, Baguio City<br>
                Phone:
                (074) 442-4938
            </address>
            <div>
                <strong>Jearson Gomez</strong><br>
                <a href="mailto:admin@4ksoftware.io" class="text-danger">admin@4ksoftware.io</a>
            </div>     
          </div>
          <div class="col">
            <div id="map"></div>
          </div>
        </div>
      </div>
    </div>  
  </div>
</section>

@endsection

@section('scripts')

  <script src="https://cdn.jsdelivr.net/algoliasearch/3/algoliasearch.min.js"></script>
  <script src="https://cdn.jsdelivr.net/hogan.js/3.0/hogan.min.js"></script>
  <script src="https://cdn.jsdelivr.net/autocomplete.js/0/autocomplete.min.js"></script>
  <script>
      function initMap() {
        var uluru = {lat: 16.3995115, lng: 120.5664172};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 13,
          center: uluru,
          scrollwheel: false
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map
        });
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB4ZLgX2yVPmZ7-oZHmFWbso43fBfOhu3c&callback=initMap">
    </script>
  <script>
    var client = algoliasearch('J7Y2ZXN5MT','ec1be8d30a7da7f75b365bcc7b435623');
    var properties = client.initIndex('properties');

    var template_property = Hogan.compile('<div class="algolia-search">' +
            '<a href="{{ url('/') }}/properties/@{{slug}}"><span>@{{{ _highlightResult.address.value }}} </span><span>@{{{ _highlightResult.name.value }}}</span></a>' +
          '</div>');
            
    autocomplete('#aa-search-input', {}, [
        {
          source: autocomplete.sources.hits(properties, { hitsPerPage: 3 }),
          displayKey: 'name',
          templates: {
            header: '<div class="aa-suggestions-category">Properties</div>',
            suggestion: function(suggestion) {

              return template_property.render(suggestion);
            }
          }
        }
    ]);
  </script>
  
@endsection
