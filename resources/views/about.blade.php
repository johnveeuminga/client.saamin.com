@extends('layouts.master')

@section('content')
<section>
	<div class="row">
		<div class="container pt-2">
			<div class="col-md-9 offset-md-2">
				<h2 class="mb-2">About Us </h2>
				<div class="card card-outline-secondary p-1">
						<img src="../img/large.png" class="pull-left card-img-top img-fluid pr-2">
						<p>
							Has the largest collection million dollar listings in Massachusetts and serves as a direct conduit to luxury home buyers and sellers across the globe- with an  average over 3000 visitors per week. We translate listing information into nearly 60 different native languages and new luxury listings are added every 24 hrs. In essence, we do the hard work, so you don't have to! As a Luxury Home Buyer, you will gain exclusive inside access to the most impressive, luxury real estate listings and luxury homes for sale throughout Massachusetts. And if you're a luxury home seller, you will undoubtedly see why people choose to list with us, as you will receive unparalleled exposure for your luxury property. 
						</p>
						<p>
							LuxuryRealEstateInMA.com - is ranked as one of the TOP 20 Luxury Real Estate websites for Massachusetts and was designed and built by, Top Producing Luxury Agent Evan S. Walsh of  The Walsh Team , named a "Best Agents in America", by The Wall Street Journal's REAL Trends Magazine for 2013.
						</p>
				</div>
			</div>
			<div class="col-md-9 offset-md-2">
				<h2 class="mb-2"> Founder</h2>
				<div class="card card-outline-secondary p-1">
					<img src="../img/founder.png" class="float-xs-left card-img img-fluid pr-2 ">
					<h3>First Name M. Last
					<br>
					<small>Company Position</small></h3>
					<div class="card-blockquote pt-1">
						<p>									
							Lorem ipsum dolor sit amet
							Consectetur adipiscing elit
							Integer molestie lorem at massa
							Facilisis in pretium nisl aliquet
							Nulla volutpat aliquam velit
							Phasellus iaculis neque
							Purus sodales ultricies
							Vestibulum laoreet porttitor sem
							Ac tristique libero volutpat at
							Faucibus porta lacus fringilla vel
							Aenean sit amet erat nunc
							Eget porttitor lorem
						</p>

					</div>
					<img src="../img/founder.png" class="float-xs-left card-img img-fluid pr-2">
					<h3 class="pt-2">First Name M. Last
					<br>
					<small>2 Company Position</small></h3>
					<div class="card-blockquote pt-1">
						<p>									
							Lorem ipsum dolor sit amet
							Consectetur adipiscing elit
							Integer molestie lorem at massa
							Facilisis in pretium nisl aliquet
							Nulla volutpat aliquam velit
							Phasellus iaculis neque
							Purus sodales ultricies
							Vestibulum laoreet porttitor sem
							Ac tristique libero volutpat at
							Faucibus porta lacus fringilla vel
							Aenean sit amet erat nunc
							Eget porttitor lorem
						</p>

					</div>
				</div>

			</div>
		</div>
	</div>

<!-- 	<div class="col-xs-12 col-md-8 first-row ">
		<h2 class="text-xs-center header">About Us</h2>
		<hr>
		<h4 class="name">"Saamin.com"</h4><p>has the largest collection million dollar listings in Massachusetts and serves as a direct conduit to luxury home buyers and sellers across the globe- with an  average over 3000 visitors per week. We translate listing information into nearly 60 different native languages and new luxury listings are added every 24 hrs. In essence, we do the hard work, so you don't have to! As a Luxury Home Buyer, you will gain exclusive inside access to the most impressive, luxury real estate listings and luxury homes for sale throughout Massachusetts. And if you're a luxury home seller, you will undoubtedly see why people choose to list with us, as you will receive unparalleled exposure for your luxury property. 

		LuxuryRealEstateInMA.com - is ranked as one of the TOP 20 Luxury Real Estate websites for Massachusetts and was designed and built by, Top Producing Luxury Agent Evan S. Walsh of  The Walsh Team , named a "Best Agents in America", by The Wall Street Journal's REAL Trends Magazine for 2013.</p>
	</div> -->
</section>
@endsection