@extends('layouts.master')
@section('title')
    <title>Update {{ $property->name }}</title>
@endsection
@section('content')
    <div class="container">
        <br>
        <div class="card">
            <div class="card-header">
                Update {{ $property->name }}
            </div>
            <div class="card-block">
                <form method="POST" class="propertyformedit" action='{{url("properties/$property->slug")}}'  
                enctype="multipart/form-data" files='true'>
                {{ csrf_field() }}
                {{method_field('PUT')}}
                @include('components.success')
                    <div class="form-group row {{ $errors->has('name')? ' has-error' : '' }}">
                        <label for="name" class="col-sm-2 form-control-label">Property Name</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputEmail3" placeholder="Name" name='name' value="{{$errors->has('name') ? old('name'):  $property->name }}">
                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row {{ $errors->has('address') ? ' has-error' : '' }}">
                        <label for="address" class="col-sm-2 form-control-label">Address</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="address" placeholder="Address" name='address' value="{{$errors->has('address') ? old('address'):  $property->address }}">
                            @if ($errors->has('address'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('address') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row {{ $errors->has('city_services')? ' has-error' : '' }}">
                        <label for="city_services" class="col-sm-2 form-control-label">City Services</label>
                        <div class="col-sm-3">
                            <input type="number" step='0.01' class="form-control" id="city_services" placeholder="Php" name="city_services" value="{{$errors->has('city_services') ? old('city_services'):  $property->city_services}}">
                            @if ($errors->has('city_services'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('city_services') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row {{ $errors->has('description') ? ' has-error' : '' }}">
                        <label for="description" class="col-sm-2 form-control-label">Description</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="description" placeholder="Description" name='description' value="{{$errors->has('description') ? old('description'):  $property->description}}">
                            @if ($errors->has('description'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('description') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row {{ $errors->has('size') ? ' has-error' : '' }}">
                        <label for="floor_size" class="col-sm-2 form-control-label">Floor Size</label>
                        <div class="col-sm-10"> 
                            <input type="number" step='0.01' class="form-control" id="floor_size" placeholder="Floor Size" name='floor_size' value="{{$errors->has('size') ? old('address'):  $property->size }}">
                            @if ($errors->has('size'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('size') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row {{ $errors->has('price') ? ' has-error' : '' }}">
                        <label for="price" class="col-sm-2 form-control-label">Price</label>
                        <div class="col-sm-3">
                            <input type="number" step='0.01' class="form-control" id="price" placeholder="Price" name="price" value="{{$errors->has('price') ? old('price'):  $property->price }}">
                            @if ($errors->has('price'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('price') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row {{ $errors->has('bedrooms') ? ' has-error' : '' }}">
                        <label for="bedrooms" class="col-sm-2 form-control-label">Number of Bedrooms</label>
                        <div class="col-sm-3">
                            <input type="number" step='1' class="form-control" id="bedrooms" placeholder="Number of Bedrooms" name="bedrooms" value="{{$errors->has('bedrooms') ? old('bedrooms'):  $property->bedrooms }}">
                            @if ($errors->has('bedrooms'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('bedrooms') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row {{ $errors->has('bathrooms') ? ' has-error' : '' }}">
                        <label for="price" class="col-sm-2 form-control-label">Number of Bathrooms</label>
                        <div class="col-sm-3">
                            <input type="number" step='1' class="form-control" id="bathrooms" placeholder="Number of Bathrooms" name="bathrooms" value="{{$errors->has('bathrooms') ? old('bathrooms'):  $property->bathrooms }}">
                            @if ($errors->has('bathrooms'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('bathrooms') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2">Location</label>
                        <label for="latitude" class="col-sm-2 form-control-label">Latitude</label>
                        <div class="col-sm-2 {{ $errors->has('latitude') ? ' has-error' : '' }}">
                            <input type="number" step='.000000000000000001' class="form-control" id="latitude" placeholder="Latitude" name="latitude" value="{{$errors->has('lat') ? old('lat'):  $property->_geoloc->lat }}">
                            @if ($errors->has('latitude'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('latitude') }}</strong>
                                </span>
                            @endif
                        </div>
                        <label for="longitude" class="col-sm-2 form-control-label">Longitude</label>
                        <div class="col-sm-2 {{ $errors->has('longitude') ? ' has-error' : '' }}">
                            <input type="number" step='.000000000000000001' class="form-control" id="longitude" placeholder="Longitude" name="longitude" value="{{$errors->has('long') ? old('longitude'):  $property->_geoloc->lng }}">
                            @if ($errors->has('long'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('longitude') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div id="map"></div>
                    <hr>
                    <input type="hidden" name="zoom" id="zoom" value="19">
                    <div class="form-group">
                        <button type="submit" class="btn btn-info">Update Property</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script>
    var marker;
    function initMap() {
        var lat = document.getElementById('latitude');
        var lng = document.getElementById('longitude');
        var uluru = {lat: {{$property->_geoloc->lat}}, lng: {{$property->_geoloc->lng}} };
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: {{$property->zoom}},
            center: uluru,
            scrollwheel: false,
            draggableCursor: 'pointer',
             draggingCursor: 'default'
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map
        });

        function placeMarker(location){
            if(marker == undefined){
                marker = new google.maps.Marker({
                    position: location,
                    map:map,
                })
            }else{
                marker.setPosition(location);
            }
        }
        google.maps.event.addListener(map, 'zoom_changed', function() {
            document.getElementById("zoom").value = map.getZoom();
        });
        google.maps.event.addDomListener(lat, 'change', function(){
            var newPosition;
            if(lng.value!=''){

                newPosition = new google.maps.LatLng(parseFloat(lat.value), parseFloat(lng.value));
                map.setCenter(newPosition);
                placeMarker(newPosition);
            }
        });
        google.maps.event.addDomListener(lng, 'change', function(){
            var newPosition;
            if(lat.value!=''){
                 newPosition = new google.maps.LatLng(parseFloat(lat.value), parseFloat(lng.value));
                map.setCenter(newPosition);
                placeMarker(newPosition);
            }
        });
        google.maps.event.addListener(map, 'click', function(event) {
            placeMarker(event.latLng)
            document.getElementById('latitude').value=event.latLng.lat().toFixed(6);
            document.getElementById('longitude').value=event.latLng.lng().toFixed(6);
        });
    }
</script>
<script async defer
 src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB4ZLgX2yVPmZ7-oZHmFWbso43fBfOhu3c&callback=initMap">
</script>
@endsection
