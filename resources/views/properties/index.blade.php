@extends('layouts.master')

@section('title')
    <title>Properties</title>
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col aisdemo--left-column mt-3">
            <div class="ais-search-box">
                <input type="text" id="search-box" class="p-4" />
            </div>
            <div class="aisdemo-filters">
                <div class="row aisdemo-filter">
                    <div class="col-sm-3 aisdemo-filter-title">Price Range</div>
                    <div class="col-sm-9" id="price"></div>
                </div>

                <div class="row aisdemo-filter">            
                    <div class="col-sm-3 aisdemo-filter-title">Bedrooms</div>
                    <div id="bedrooms"></div>
                </div>

                <div class="row aisdemo-filter">
                    <!--  bathrooms -->          
                    <div class="col-sm-3 aisdemo-filter-title">Bathrooms</div>
                    <div id="bathrooms"></div>
                </div>

                <div class="row">
                    <div id="stats" class="p-2 bg-warning text-white text-center"></div>                    
                </div>
            </div>
        </div>

        <!-- Right Column -->
        <div class="col aisdemo--right-column">
            <div id="map"></div>
        </div>
    </div>
</div>

<!-- results -->
<div id="results">
    <div class="container">
        <div class="row">
            <div id="hits" class="w-100"></div>
        </div>
        <div class="row">
            <div id="pagination" class="text-center"></div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB4ZLgX2yVPmZ7-oZHmFWbso43fBfOhu3c"></script>
    <script src="https://cdn.jsdelivr.net/instantsearch.js/1/instantsearch.min.js"></script>
    <script src="https://cdn.jsdelivr.net/instantsearch-googlemaps/1/instantsearch-googlemaps.min.js"></script>
    <script>
        var search = instantsearch({
            appId: $('meta[name="app-id"]').attr('content'),
            apiKey: $('meta[name="api-key"]').attr('content'),
            indexName: $('meta[name="alg-table"]').attr('content')
        });

        var hitTemplate =
          '<a class="hit clearfix" href="{{ url("properties") }}/@{{slug}}">' +
            '<div class="pull-left">' +
                '<img class="picture" src="@{{image_thumbnail}}" />' +
            '</div>' +
            '<div class="pull-left ml-3">' +
                '<h2 class="h4 media-heading">@{{{_highlightResult.name.value}}}</h2>' +
                '<p><i class="fa fa-map-marker" aria-hidden="true"></i> @{{{_highlightResult.address.value}}}</p>' +
                '<p><i class="fa fa-bed" aria-hidden="true"></i> @{{bedrooms}} - <i class="fa fa-bath" aria-hidden="true"></i> @{{bathrooms}}</p>' +
            '</div>' +
            '<div class="pull-right">'+
                '<h2 class="h4 text-danger"><i class="fa fa-credit-card" aria-hidden="true"></i> P@{{price_formatted}}</h2>' +
            '</div>'+
          '</a><hr>';

        var noResultsTemplate = '<div class="text-center">No results found matching.</div>';

        search.addWidget(
            instantsearch.widgets.searchBox({
                container: '#search-box',
                placeholder: 'Search for properties'
            })
        );

        search.addWidget(
            instantsearch.widgets.stats({
                container: '#stats'
            })
        );

        search.addWidget(
            instantsearch.widgets.hits({
                container: '#hits',
                hitsPerPage: 12,
                templates: {
                    empty: noResultsTemplate,
                    item: hitTemplate
                }
            })
        );

        search.addWidget(
            instantsearch.widgets.numericSelector({

                container: '#bedrooms',
                attributeName: 'bedrooms',
                operator: '>=',
                options: [
                    { label: '1 bedroom', value: 1},
                    { label: '2 bedrooms', value: 2},
                    { label: '3 bedrooms', value: 3},
                    { label: '4 bedrooms', value: 4},
                    { label: '5 bedrooms', value: 5},
                    { label: '6 bedrooms', value: 6}
                ]
            })
        );

        search.addWidget(
            instantsearch.widgets.numericSelector({

                container: '#bathrooms',
                attributeName: 'bathrooms',
                operator: '>=',
                options: [
                    { label: '1 bathrooms', value: 1},
                    { label: '2 bathrooms', value: 2},
                    { label: '3 bathrooms', value: 3},
                    { label: '4 bathrooms', value: 4},
                    { label: '5 bathrooms', value: 5},
                    { label: '6 bathrooms', value: 6}
                ]
            })
        );

        search.addWidget(
            instantsearch.widgets.rangeSlider({
                container: '#price',
                attributeName: 'price',
                tooltips:{
                    format: function(rawValue){
                        return 'P' + Math.round(rawValue).toLocaleString();
                    }
                },
                pips: false,
            })
        );

        search.addWidget(
            instantsearch.widgets.googleMaps({
                container: document.querySelector('#map'),
                attributeName: ['lat','long']
            })
        );

        search.addWidget(
            instantsearch.widgets.pagination({
                container: '#pagination',
                scrollTo: '#results',
                cssClasses: {
                    root: 'pagination',
                    active: 'active'
                }
            })
        );

        search.start();

        function getTemplate(templateName) {
            return document.getElementById(templateName + '-template').innerHTML;
        }

    </script>
@endsection