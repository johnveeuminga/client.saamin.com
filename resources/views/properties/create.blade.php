@extends('layouts.master')

@section('stylesheets')
@section('content')
<div class=" col-md-12 clearfix">
    @if(count($errors) > 0)
        <div class="alert alert-danger" role="alert">
            @foreach($errors->all() as $error)
                <p> {{ $error }} </p>
            @endforeach
        </div>
    @endif
    <h2 class="text-xs-center p-3 text-white bg-info">Create Property</h2>
    <div class="col-md-6 p-2">    
    <form method="POST" class="propertyform" action='{{url('/properties')}}' enctype="multipart/form-data" files="true" multiple = "true">

        {{csrf_field()}}

        <div class="form-group row {{ $errors->has('name')? ' has-error' : '' }}">
            <label for="name" class="col-sm-2 form-control-label">Property Name</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="inputEmail3" placeholder="Name" name='name' value ="{{ old('name') }}">
            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
            </div>
        </div>
        <div class="form-group row {{ $errors->has('address')? ' has-error' : '' }}">
            <label for="address" class="col-sm-2 form-control-label">Address</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="address" placeholder="Address" name='address' value="{{ old('address') }}"> 
                @if ($errors->has('address'))
                    <span class="help-block">
                        <strong>{{ $errors->first('address') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group row {{ $errors->has('floor_size')? ' has-error' : '' }}">
            <label for="floor_size" class="col-sm-2 form-control-label">Floor Size</label>
            <div class="col-sm-10">
                <input type="number" step='0.01' class="form-control" id="floor_size" placeholder="Floor Size" name='floor_size' value="{{ old('floor_size') }}">
                @if ($errors->has('floor_size'))
                    <span class="help-block">
                        <strong>{{ $errors->first('floor_size') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group row {{ $errors->has('bedrooms')? ' has-error' : '' }}">
            <label for="bedrooms" class="col-sm-2 form-control-label">Number of </label>
            <div class="col-sm-4">
                <input type="number" step='1' class="form-control" id="bedrooms" placeholder="Bedrooms" name="bedrooms" value="{{ old('bedrooms') }}">
                @if ($errors->has('bedrooms'))
                    <span class="help-block">
                        <strong>{{ $errors->first('bedrooms') }}</strong>
                    </span>
                @endif
            </div>
            <div class="col-sm-4">
                <input type="number" step='1' class="form-control" id="bathrooms" placeholder="Bathrooms" name="bathrooms" value="{{ old('bathrooms') }}">
                @if ($errors->has('bathrooms'))
                    <span class="help-block">
                        <strong>{{ $errors->first('bathrooms') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2">Location</label>
            <div class="col-sm-4 {{ $errors->has('lat')? ' has-error' : '' }}">
                <input type="number" step="any" class="form-control" id="latitude" placeholder="Latitude" name="latitude" value="{{ old('latitude') }}">
                @if ($errors->has('latitude'))
                    <span class="help-block">
                        <strong>{{ $errors->first('latitude') }}</strong>
                    </span>
                @endif
            </div>
            <div class="col-sm-4 {{ $errors->has('long')? ' has-error' : '' }}">
                <input type="number"  step="any" class="form-control" id="longitude" placeholder="Longitude" name="longitude" value="{{ old('longitude') }}">
                @if ($errors->has('longitude'))
                    <span class="help-block">
                        <strong>{{ $errors->first('longitude') }}</strong>
                    </span>
                @endif
            </div>
        </div>
         <div class="form-group row {{ $errors->has('price')? ' has-error' : '' }}">
            <label for="price" class="col-sm-2 form-control-label">Price</label>
            <div class="col-sm-3">
                <input type="number" step='0.01' class="form-control" id="price" placeholder="Php" name="price" value="{{ old('price') }}">
                @if ($errors->has('price'))
                    <span class="help-block">
                        <strong>{{ $errors->first('price') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group row {{ $errors->has('city_services')? ' has-error' : '' }}">
            <label for="city_services" class="col-sm-2 form-control-label">City Services</label>
            <div class="col-sm-3">
                <input type="number" step='0.01' class="form-control" id="city_services" placeholder="Php" name="city_services" value="{{ old('city_services') }}">
                @if ($errors->has('city_services'))
                    <span class="help-block">
                        <strong>{{ $errors->first('city_services') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group row {{ $errors->has('description')? ' has-error' : '' }}">
            <label for="description" class="col-sm-2 form-control-label">Description</label>
            <div class="col-sm-10">
                <textarea type="text" class="form-control" id="description" placeholder="Description" name='description' value="{{ old('description') }}"></textarea>
                @if ($errors->has('description'))
                    <span class="help-block">
                        <strong>{{ $errors->first('description') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <input type="hidden" name="zoom" id="zoom" value="19">
        <div class="mb-2" id="map"></div>
        <button class="btn btn-outline-success btn-block" type="submit">Create Property</button>
    </form>
</div>
@endsection

@section('scripts')
  <script>
    navigator.geolocation.getCurrentPosition(function(location) {
          console.log(location.coords.latitude);
          console.log(location.coords.longitude);
          console.log(location.coords.accuracy);
    });
    var marker;
      function initMap() {
        var lat = document.getElementById('latitude');
        var lng = document.getElementById('longitude');
        var uluru = {lat: 16.3995115, lng: 120.5664172};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 19,
          center: uluru,
          scrollwheel: false,
          draggableCursor: 'pointer',
          draggingCursor: 'default'
        });
        console.log(map.getZoom());

        function placeMarker(location){
            if(marker == undefined){
                marker = new google.maps.Marker({
                    position: location,
                    map:map,
                })
              }else{
                marker.setPosition(location);
              }
        }

        google.maps.event.addListener(map, 'zoom_changed', function() {
            document.getElementById("zoom").value = map.getZoom();
        });

        google.maps.event.addDomListener(lat, 'change', function(){
            var newPosition;
            if(lng.value!=''){

                newPosition = new google.maps.LatLng(parseFloat(lat.value), parseFloat(lng.value));
                map.setCenter(newPosition);
                placeMarker(newPosition);
            }
        });

        google.maps.event.addDomListener(lng, 'change', function(){
            var newPosition;
            if(lat.value!=''){
                 newPosition = new google.maps.LatLng(parseFloat(lat.value), parseFloat(lng.value));
                map.setCenter(newPosition);
                placeMarker(newPosition);
            }
        });
        google.maps.event.addListener(map, 'click', function(event) {
            placeMarker(event.latLng)
            document.getElementById('latitude').value=event.latLng.lat().toFixed(6);
            document.getElementById('longitude').value=event.latLng.lng().toFixed(6);
          });
        }
    </script>
    <script async defer
     src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB4ZLgX2yVPmZ7-oZHmFWbso43fBfOhu3c&callback=initMap">
    </script>
@endsection
