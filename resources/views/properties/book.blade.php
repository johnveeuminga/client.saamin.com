@extends('layouts.master')
@section('title')
	<title>Book {{ $property->name }}</title>
@endsection
@section('content')
	<br>
	<div class="container">
		<div class="col-sm-12">
			
			<div class="col-sm-7">
				<div class="card card-block">

					<h4 class="card-title">About your Rent</h4>
	
					<form action="{{ url('/properties/' .$property->slug .'/book') }}" method="POST">
						{{ csrf_field() }}
						<!-- guests -->
			            <div class="form-group">
			                <label for="guests">Guests</label>
			                <div class="col-sm-12">
			                    <select class="form-control" >
			                      <option>1 guest</option>
			                      <option>2 guests</option>
			                    </select> 
			                </div>
			          
			            </div>
			            <br><br>
						<p class="card-text">Say hello to your host and tell them why your renting</p>
						<textarea name="message" class="form-control" rows="3" placeholder="Message your Host!"></textarea>
						
						<div class="form-group">
							<br>
							<button type="submit" class="btn btn-sm btn-danger">Send Request</button>
						</div>

					</form>
					
				</div>
			</div>

			<div class="col-sm-5">
				<img class="card-img-top img-fluid" src="{{ $property->cover_image }}" alt="Card image cap">
				<div class="card card-block">
					@include('properties.layouts.details')

					<hr>
					<p class="text-muted"><strong>Price :</strong> {{ $property->price }}</p>
					<p class="text-muted"><strong>City Services :</strong> {{ $property->city_services }}</p>

					<p><strong>Total : PHP {{ $property->price + $property->city_services }}</strong></p>
				</div>
			</div>
		</div>
	</div>
@endsection