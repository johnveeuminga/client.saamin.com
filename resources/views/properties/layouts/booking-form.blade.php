<div class="row">
    <div class="col-md-12">
        @include('errors.error')
        <div class="card">
            <div class="card-block">
                <form method="GET" action="{{ route('properties.book', $property->slug) }}">
                    <date-pick :property-id="{{ $property->id }}" ></date-pick>
                      @foreach($property->agent->users as $user)
                        <input type="hidden" name="recipient" value="{{$user->email}}">
                      @endforeach
                </form>
            </div>
        </div>
    </div>
</div>

