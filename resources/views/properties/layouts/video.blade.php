@if($new)
    <div class="dz-video-container" style="height: 100%;">
        <form class="dropzone" id="video-dropzone" action="{{url("properties/$property->id/video")}}" method="POST">
            <div class="dz-message" data-dz-message><span>Click or drag the video here for upload.</span></div>
            <input type="hidden" value="{{$property->name}}" id="property_name"> 
        </form>
    </div> 
@else
    @if(!is_null($property->video))
        <div class="embed-responsive embed-responsive-16by9">
            <video class="embed-responsive-item" controls>
                <source src='{{url($property->video->path)}}' type="video/mp4"> 
            </video>
            <input type="hidden" name="" value={{$property->video->id}}>
            @if(Auth::check())
                @if(Auth::user()->userable_type=='App\UserAgent'
                    && Auth::user()->userable_id==$property->user_agent_id)
                    <div class="overlay float-xs-right">
                        <div class="dropdown float-xs-right">
                            <button class="btn main-btn dropdown-toggle" type="button" id="dropdownMenuButton"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-pencil-square-o"></i>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                              <a class="dropdown-item" href="#" id="upload-video">Update Video</a>
                              <a class="dropdown-item" href="#" data-toggle="modal" data-target="#removeVideoModal">Remove Video</a>
                            </div>
                        </div>
                    </div>
                @endif
            @endif
        </div>
    @else
        <div class="col-md-12">
            <p class="text-xs-center">No videos  uploaded yet.</p>
            @if(Auth::check())
                @if(Auth::user()->userable_type=='App\UserAgent' && Auth::user()->userable_id==$property->user_agent_id)
                    <div class="upload-new-vid-container">
                        <a href="#" id="upload-new-video" class="upload-new-video" data-toggle="modal" data-target="#add-video-modal">
                            Click here to upload a new video
                        </a>
                    </div>
                @endif
            @endif
        </div>
    @endif
@endif