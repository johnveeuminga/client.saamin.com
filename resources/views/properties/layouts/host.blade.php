@foreach($property->agent->users as $user)
    <p class="h6">
        Your Host: {{$user->firstname }} {{ $user->lastname}} 
    </p>
    <p class="small text-muted">Address - {{ $user->address }}</p>
    <div>
        <a href="{{ route('messages.show', $user->id) }}" class="btn btn-sm btn-info">
            Contact Host
        </a>    
    </div>
@endforeach

