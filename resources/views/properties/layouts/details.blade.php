<div>
    <h5 class="h4">{{$property->name}}</h5>
    <p class="text-muted">Address: {{$property->address}}</p>
    <p class="small text-muted">{{$property->description}}</p>
    <p class="small text-muted">
        <i class="fa fa-credit-card fa-2x" aria-hidden="true"></i> 
         Price: P{{$property->price_formatted}}
    </p>
    <hr>
    <p class="small text-muted">
        <i class="fa fa-cubes fa-2x" aria-hidden="true"></i> 
         Bathrooms:  {{$property->bathrooms}}
    </p>
    <hr>
    <p class="small text-muted">
        <i class="fa fa-bed fa-2x" aria-hidden="true"></i> 
         Bedrooms:  {{$property->bedrooms}}
    </p>
    
    @if(Auth::check())
        @if(Auth::user()->userable_id !=$property->user_agent_id)
            <hr>
            @include('properties.layouts.host')
        @endif
    @endif
</div>
