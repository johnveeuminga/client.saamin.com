@extends('layouts.master')
@section('title')
    <title>{{ $property->name }}</title>
@endsection
@section('content')
    <img src="{{ $property->cover_image }}">
    <section class="container mt-3">
        <div class="row">
            @if($new)
                <div class="alert alert-info" role="alert">
                    <h2 class="text-xs-center" style=""><strong>Step 2: Set the scene - </strong>Add your property's photos and videos. </h2>        
                </div>
            @endif
            <div class="container">
                @include('components.success')
                <div class="row">
                    <div class="col-md-6">
                        @include('properties.layouts.details')
                    </div>
                    <div class="col-md-6">
                        @include('properties.layouts.video')
                    </div>
                </div>
                <hr>
                @if(!$propertyAlreadyBooked)
                    @include('properties.layouts.booking-form')
                @endif
            </div>
        </div>

    <!-- User must be login to be able to update and delete property -->
    @if(Auth::check())
        @if(Auth::user()->userable_type=='App\UserAgent' && Auth::user()->userable_id==$property->user_agent_id)
            <div class="float-xs-right">
                <a class="btn btn-primary" href='{{url("properties/$property->slug/edit")}}' role="button">Update</a>
                @if($new)
                <a class="btn btn-primary" id="dz-submit" href='{{url("properties/$property->slug")}}' style="color:#fff;">Save</a>
                  
                @endif
            </div>
        @endif
    @endif
    </div>
    <!-- end of details -->
    <hr>
    <h1 class="h4">Property Photos</h1>
    <hr>
    @if(!is_null($property->photos))
      <div class="card-deck-wrapper photo-card">
        <div class="card-deck">
            @if(count($property->photos)==0)
              
              @if(!$new)
                
                <p>No photos uploaded yet for this property.</p>

                @if(Auth::user()->userable_type=='App\UserAgent' && Auth::user()->userable_id==$property->user_agent_id)
                  <a href="#" id="upload-new-photo" class="upload-new-photo" data-toggle="modal" data-target="#add-photo-modal">
                  <div class="row">
                      <div class="col-md-12 upload-new-photo-container">
                          Click to add photos.
                      </div> 
                  </div>
                  </a>
                @endif
              
              @endif

            @else

              <div class="row">
                
                @foreach($property->photos as $index=>$photo)
                  <div class="col-md-4">
                    <div class="image-container">
                      <a href="{{ url($photo->path) }}" data-toggle="lightbox" data-gallery="example-gallery">
                        <img class="img-fluid" src='{{ url($photo->path) }}'>
                      </a>

                      @if(Auth::check())
                        @if(Auth::user()->userable_type=='App\UserAgent' && Auth::user()->userable_id==$property->user_agent_id)
                          <div class="overlay float-xs-right">
                            <div class="dropdown float-xs-right">
                              <button class="btn main-btn dropdown-toggle" type="button" id="photo-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-pencil-square-o"></i>
                              </button>
                              <div class="dropdown-menu" aria-labelledby="photo-dropdown">
                                <a class="dropdown-item upload-photo" href="#" id="upload-photo" name="upload-photo">Update Photo</a>
                                <a class="dropdown-item delete-photo" href="#" id="delete-photo">Delete Photo</a>
                              </div>
                            </div>
                          </div>
                        @endif
                      @endif

                      <span class="space"></span>
                      <input type="hidden" name="photo_id" value="{{$photo->id}}" id="photo_id{{$index}}">
                    </div>
                  </div>
                @endforeach
              </div>

              @if(Auth::check())

                @if(Auth::user()->userable_type=='App\UserAgent' && Auth::user()->userable_id==$property->user_agent_id)
                  <a href="#" id="upload-new-photo" class="upload-new-photo" data-toggle="modal" data-target="#add-photo-modal">
                  <div class="row">
                    <div class="col-md-12 upload-new-photo-container">
                      Click to add photos.
                    </div> 
                  </div>
                </a>
                @endif

              @endif

          @endif
        </div>
        
        @if($new)
          <form class="dropzone" action='{{url("properties/$property->id/photos")}}' method="POST" id="photos-dropzone">
          </form>
          <input type="hidden" value="{{$property->name}}" id="property_name"> 
        @endif

      </div>
    @else
      <p class="text-xs-center">No Photos Uploaded</p>
    @endif

    <hr> 
    <h1 class="h4">Map Location</h1>
    <hr>
    <div id="map"></div>

    {{-- Modal for adding photo--}}
        <div class="modal fade" id="add-photo-modal">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Upload Photo</h4>
              </div>
              <div class="modal-body">
                <form class="dropzone" id="upload-photo" action='{{url("properties/$property->id/photos")}}'
                 method="POST">
                 {{ csrf_field() }}
                  <div class="dz-message" data-dz-message><span>Click or drag the photos here for upload.</span></div>
                  <input type="hidden" value="{{$property->name}}" id="property_name"> 
                </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="upload-photo-submit">Upload</button>
              </div>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

     {{-- Modal for updating photo--}}
        <div class="modal fade" id="update-photo-modal">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Update Photo</h4>
              </div>
              <div class="modal-body">
                <form class="dropzone" id="update-photo-form" action='{{url("properties/$property->id/photos")}}' 
                  method="POST">
                  <div class="dz-message" data-dz-message><span>Click or drag the photos here for upload.</span></div>
                  <input type="hidden" value="{{$property->name}}" id="property_name"> 
                </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="update-photo-submit">Upload</button>
              </div>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

    {{-- Modal for adding video --}}
        <div class="modal fade" id="add-video-modal">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Upload Video</h4>
              </div>
              <div class="modal-body">
                <form class="dropzone" id="add-video-dropzone-modal" action="{{url("properties/$property->id/video")}}" method="POST">
                  <div class="dz-message" data-dz-message><span>Click or drag the video here for upload.</span></div>
                  <input type="hidden" value="{{$property->name}}" id="property_name"> 
                </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" id="add-video-submit">Upload</button>
              </div>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

    {{-- Modal for updating video --}}
        <div class="modal fade" id="upload-video-modal">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Upload Video</h4>
              </div>
              <div class="modal-body">
                @if($property->video)
                  <form class="dropzone" action="{{url("properties/$property->id/video/")}}/{{$property->video->id}}" id="video-dropzone-modal">
                    <div class="dz-message" data-dz-message><span>Click or drag the video here for upload.</span></div>
                     <input type="hidden" value="{{$property->name}}" id="property_name"> 
                  </form>
                @endif
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" id="upload-video-submit">Upload</button>
              </div>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
      
        {{-- Modal for deletion of video --}}
        <div class="modal fade" id="removeVideoModal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header"></div>
                    <div class="modal-body">
                      <p>Are you sure to remove this video?</p>
                    </div>
                    <div class="modal-footer">
                        @if($property->video)
                            <form id="videoDelete" method="POST" action='{{url("properties/$property->id/video/")}}/{{$property->video->id}}'>
                            {{method_field('DELETE')}}
                            {{csrf_field()}}
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                                <button type="submit" class="btn btn-danger" form="videoDelete">Yes</button>
                            </form>
                        @endif
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal -->
        </div>

        <div class="modal fade" id="removePhotoModal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header"></div>
                    <div class="modal-body">
                      <p>Are you sure to remove this Photo?</p>
                    </div>
                    <div class="modal-footer">
                      <form id="photo-delete" method="POST" action='#'>
                        {{method_field('DELETE')}}
                        {{csrf_field()}}
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                        <button type="submit" class="btn btn-danger" form="photo-delete">Yes</button>
                      </form>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
  </section>
@endsection

@section('scripts')
<script>
    function initMap() {
        var uluru = {lat: {{$property->_geoloc->lat}}, lng: {{$property->_geoloc->lng}} };
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: {{$property->zoom}},
            center: uluru,
            scrollwheel: false,
            draggableCursor: 'default',
            draggingCursor: 'default'
        });
        var marker = new google.maps.Marker({
            position: uluru,
            map: map
        });
    }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB4ZLgX2yVPmZ7-oZHmFWbso43fBfOhu3c&callback=initMap">
</script>
<script>
    var property_name = $('#property_name').val();
    Dropzone.options.photosDropzone = {
        autoProcessQueue: false,
        addRemoveLinks: true,
        uploadMultiple: true,
        maxFileSize: 10,
        paramName: 'photo',
        parallelUploads: 100,
        acceptedFiles: 'image/*',
        dictRemoveFile: 'Remove',
        dictFileTooBig: 'Image is bigger than 10MB',
        dictInvalidFileType: 'File must be an image.',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        init: function(){
            var dropzoneForm = this;
            this.on("sending", function(file, xhr, formData){
              formData.append('propertyName', property_name);
            });

            $('#dz-submit').on("click", function(e) {
                e.preventDefault();
                dropzoneForm.processQueue();
                if(dropzoneForm.getQueuedFiles().length <= 0){
                    var videoDropzone = Dropzone.forElement("#video-dropzone");
                    if(videoDropzone.getQueuedFiles().length <= 0){
                       window.location.href = $('#dz-submit').attr('href');
                    }
                }
            });  
        },
        success: function(file,response) {
            var videoDropzone = Dropzone.forElement("#video-dropzone");
            videoDropzone.processQueue();
        },
        error: function(file, response){
            $(file.previewElement).find('.dz-error-message').text(response).show().css({"opacity": 70, "top": "70px"});
            $(file.previewElement).find('.dz-remove').text("Remove File");
        }
    }
    Dropzone.options.updatePhotoForm = {
            autoProcessQueue: false,
            addRemoveLinks: true,
            uploadMultiple: false,
            maxFileSize: 10,
            paramName: 'photo',
            parallelUploads: 1,
            acceptedFiles: 'image/*',
            dictRemoveFile: 'Remove',
            dictFileTooBig: 'Image is bigger than 10MB',
            dictInvalidFileType: 'File must be an image.',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        init: function(){
            var dropzoneForm = this;
            this.on("sending", function(file, xhr, formData){
                formData.append('propertyName', property_name);
            });
            $('#update-photo-submit').on("click", function(e) {
                dropzoneForm.options.url = $('#update-photo-form').attr('action');
                console.log(dropzoneForm.options.url);
                e.preventDefault();
                dropzoneForm.processQueue();
            });    
        },
        success: function(file, response){
            window.location.reload();
        },
    }
    Dropzone.options.uploadPhoto = {
        autoProcessQueue: false,
        addRemoveLinks: true,
        uploadMultiple: true,
        maxFileSize: 1,
        paramName: 'photo',
        parallelUploads: 100,
        acceptedFiles: 'image/*',
        dictRemoveFile: 'Remove',
        dictFileTooBig: 'Image is bigger than 10MB',
        dictInvalidFileType: 'File must be an image.',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        init: function(){
            var dropzoneForm = this;
            this.on("sending", function(file, xhr, formData){
                formData.append('propertyName', property_name);
            });
            $('#upload-photo-submit').on("click", function(e) {
                e.preventDefault();
                dropzoneForm.processQueue();
            });  
        },
        success: function(file, response){
            window.location.reload();
        },
        maxfilesexceeded: function(file) {
            this.removeAllFiles();
            this.addFile(file);
        }
    }
    Dropzone.options.videoDropzone = {
        autoProcessQueue: false,
        parallelUploads: 1,
        paramName: 'video',
        addRemoveLinks: true,
        acceptedFiles: 'video/*',
        maxFiles: 1,
        clickable: true,
        dictInvalidFileType: "File must be a video",
        dictRemoveFile: "Remove File",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        init: function(){
            var dropzoneForm = this;
            this.on("sending", function(file, xhr, formData){
                formData.append('propertyName', property_name);
            });
            if(dropzoneForm.getQueuedFiles().length > 0){
                window.location.href = $('#dz-submit').attr('href');
            }
        },
        success: function(file, response){
            window.location.href = $('#dz-submit').attr('href');
        },  

        error: function(file, response){
            $(file.previewElement).find('.dz-error-message').text(response).show().css({"opacity": 70, "top": "70px"});
            $(file.previewElement).find('.dz-remove').text("Remove File");
        },
        maxfilesexceeded: function(file) {
            this.removeAllFiles();
            this.addFile(file);
        }
    }
    Dropzone.options.videoDropzoneModal = {
        autoProcessQueue: false,
        parallelUploads: 1,
        paramName: 'video',
        method: 'POST',
        addRemoveLinks: true,
        acceptedFiles: 'video/*',
        maxFiles: 1,
        clickable: true,
        dictInvalidFileType: "File must be a video",
        dictRemoveFile: "Remove File",
        headers: {
           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        init: function(){
            var dropzoneForm = this;
            this.on("sending", function(file, xhr, formData){
              formData.append('propertyName', property_name);
            });

            $(document).on("click", "#upload-video-submit", function(){
              dropzoneForm.processQueue();
            })
            
        },
        success: function(file, response){
            window.location.reload();
        },
        error: function(file, response){
            $(file.previewElement).find('.dz-error-message').text(response).show().css({"opacity": 70, "top": "70px"});
            $(file.previewElement).find('.dz-remove').text("Remove File");
        },
        maxfilesexceeded: function(file) {
            this.removeAllFiles();
            this.addFile(file);
        }
    }

    Dropzone.options.addVideoDropzoneModal = {
        autoProcessQueue: false,
        parallelUploads: 1,
        paramName: 'video',
        method: 'POST',
        addRemoveLinks: true,
        acceptedFiles: 'video/*',
        maxFiles: 1,
        clickable: true,
        dictInvalidFileType: "File must be a video",
        dictRemoveFile: "Remove File",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
        init: function(){
            var dropzoneForm = this;
            this.on("sending", function(file, xhr, formData){
              formData.append('propertyName', property_name);
            });

            $(document).on("click", "#add-video-submit", function(){
              dropzoneForm.processQueue();
            })
          },
        success: function(file, response){
          window.location.reload();
        },

       error: function(file, response){
          $(file.previewElement).find('.dz-error-message').text(response).show().css({"opacity": 70, "top": "70px"});
          $(file.previewElement).find('.dz-remove').text("Remove File");
        },
        maxfilesexceeded: function(file) {
          this.removeAllFiles();
          this.addFile(file);
        }
    }
    $(document).on('click', '#upload-video', function(f) {
        $("#upload-video-modal").modal('show');
    });
    $(document).on('click', '.upload-photo', function(f) {
        var index = $(this).index('.upload-photo');
        var selected_photo = $('#photo_id'+index).val();
        f.preventDefault();
        $('#update-photo-modal').modal('show');
        $('#update-photo-form').attr(
            'action', "http://client.saamin.com/properties/{{$property->id}}/photos/"+selected_photo
        );
    });
    $(document).on('click', '.delete-photo', function(f) {
        f.preventDefault();
        var index = $(this).index('.delete-photo');
        var selected_photo = $('#photo_id'+index).val();
        $('#removePhotoModal').modal('show');
        $('#photo-delete').attr(
            'action', "http://client.saamin.com/properties/{{$property->id}}/photos/"+selected_photo
        );
    });
</script>
@endsection