@extends('layouts.master')

@section('content')
<section> 
  <div class="row bg-info p-3">
    <h1 class="text-xs-center text-white pt-3 pb-1"> Welcome to SAAMIN Help Center</h1>
    <div class="col-md-4 offset-sm-4">
       <div class="input-group pb-3">
        <input type="text" class="form-control" placeholder="Ask a question here...">
        <span class="input-group-btn">
          <button class="btn btn-secondary" type="button"><i class="fa fa-search"></i></button>
        </span>
      </div>
    </div>
  </div>
</section>

<div class="container">
  <div class="row pt-2">
    <h5 class="text-xs-left"> Frequently Asked questions</h5>
    <div id="accordion" role="tablist" aria-multiselectable="true">
      <div class="pt-1 headingOne">
        <a class="collapsed text-muted" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne ">
        How do I edit my home facts for my off market property on Saamin?
          
        </a>
      </div>
      <div id="collapseOne" class="collapse" role="tabpanel" aria-labelledby="headingOne">
          <p class="text-justify" id="collapseOne" > 
            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
          </p>
      </div>
    
     <div class="headingTwo">
        <a class="collapsed text-muted" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
        I forgot my password. How do I request my log in information?
        </a>
      </div>
      <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
          <p class="text-justify" id="collapseTwo" > 
            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
          </p>
      </div>
      <div class="pb-1 headingThree">
        <a class="collapsed text-muted" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          How do I delete my account?
        </a>
      </div>
      <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree">
          <p class="text-justify" id="collapseThree" > 
            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
          </p>
      </div>
    </div>
  </div>
</div>


@endsection