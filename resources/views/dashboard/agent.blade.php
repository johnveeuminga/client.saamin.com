@extends('layouts.master')
   
@section('content')
<section class="jumbotron">
    <div class="container-fluid">
        <div class="row pt-5 pb-5">
            <div class="col-1">
                <div class="card">
                    <img src="{{ Auth::user()->avatar }}">

                    <div class="card-block">
                        <p class="card-text"><small>{{ Auth::user()->firstname }} {{ Auth::user()->lastname }}</small></p>
                    </div>
                </div>
            </div>
            <div class="col-9">
                <div class="card">
                    <h4 class="jumbotron">Welcome {{Auth::user()->firstname}} to Saamin Dashboard</h4>
                    <div class="card-block">
                        <ul class="list-unstyled">
                            <li class="space-2">
                                <strong><a class="btn btn-info" href="{{ url('/messages') }}">Messages</a></strong>
                            </li>
                        </ul>
                    </div>    
                </div>
                <hr>
                <div class="card">
                    <h4 class="jumbotron">Notifications</h4>
                    <div class="card-block">
                        <p class="card-text">
                            <ul class="list-group">
                                <li class="list-group-item" v-for="notification in notifications.notifications">
                                    @{{ notification.data.message }}
                                    <a @click="viewNotification(notification.id)" class="btn pull-right btn-sm btn-info">View</a>
                                    <span></span>
                                </li>
                            </ul>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>     
@endsection

