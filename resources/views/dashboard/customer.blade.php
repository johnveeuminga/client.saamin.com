@extends('layouts.master')
   
@section('content')
<section class="dashboard-agent">
    <div class="container-fluid">
        <div class="row">
            <div class="col-1">
                <div class="card">
                    <img src="{{Auth::user()->avatar}}" class="card-img-top">
                    <div class="card-block">
                        <p class="card-title">{{Auth::user()->firstname}} {{Auth::user()->lastname}}</p>
                    </div>
                </div>
            </div>

            <div class="col-6">
                <div class="card">
                    <div class="card-header">
                        <h1 class="h4 text-center">Welcome {{Auth::user()->firstname}} to Saamin Dashboard</h1>
                    </div>
                    <div class="card-block">
                        <p class="card-text">
                            Check your messages for upcoming trip information
                        </p>
                        <ul class="list-unstyled">
                            <li class="space-2">
                                <strong><a class="btn btn-info" href="{{ url('/messages') }}">Messages</a></strong>
                            </li>
                        </ul>
                    </div>    
                </div>
            </div>

            <div class="col">
                <div class="card">
                    <h4 class="text-center">Notifications</h4>
                    <div class="card-block">
                        <p class="card-text">
                            <ul class="list-group">
                                <li class="list-group-item" v-for="notification in notifications.notifications">
                                    @{{ notification.data.message }}
                                    <a @click="viewNotification(notification.id)" class="btn pull-right btn-sm btn-info">View</a>
                                    <span></span>
                                </li>
                            </ul>
                        </p>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>     
@endsection

