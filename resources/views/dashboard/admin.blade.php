@extends('layouts.master')
   
@section('content')
<nav class="navbar navbar-dark bg-inverse">
    <div class="container">
        <div class=" av navbar-nav" role="group">
            <a class="nav-item nav-link active" data-toggle="tooltip" data-placement="top" title="New Property">
                Dashboard
            </a>
            <a href="" class="nav-item nav-link" data-toggle="tooltip" data-placement="top" title="Inbox">
                Inbox&nbsp;
                <span class="tag tag-default pull-right">1</span>
            </a>
            <a class="nav-item nav-link" data-toggle="tooltip" data-placement="top" title="New Property">
                New Property
            </a>
            <a class="nav-item nav-link" data-toggle="tooltip" data-placement="top" title="Account Setting">
                Profile
            </a>
            <a class="nav-item nav-link" data-toggle="tooltip" data-placement="top" title="Account Setting">
                Account
            </a>
        </div>
        <form class="form-inline float-xs-right">
            <input class="form-control" type="text" placeholder="Search">
            <button class="btn btn-outline-success" type="submit"><i class="fa fa-search"></i></button>
        </form>
    </div>
</nav>  

<section class="dashboard-agent">
    <div class="row">
        <div class="col-md-3">
        <div class="panel-dashboard agent">
            <div class="agent-image">
                <a href=""><img src="../img/user.png" class="img-responsive"></a>
                <div class="add-profile-photo">
                    <a href="#" class="btn btn-secondary btn-contrast btn-block"><i class="fa fa-camera"></i>
                        Add Profile Photo
                    </a>
                </div>
            </div> 
        <div class="panel-profile-agent">
            <h2 class="text-xs-center">{{Auth::user()->firstname}}</h2>
            <ul class="list-profile-agent text-xs-center">
                <li>
                    <a  href="" class="btn btn-secondary">View Profile</a>
                </li>
                <li>
                    <a href="" class="btn btn-outline-success mt-1">Complete Profile</a>
                </li>
            </ul>
        </div> 
      </div>
    </div>
    <div class="col-md-9">
        <div class="card">
            <h4 class="jumbotron">Welcome {{Auth::user()->firstname}} to Saamin Dashboard</h4>
            <div class="card-block">
                <p class="card-text">
                    Check your messages, view upcoming trip information, and find travel inspiration all from your dashboard. Before booking your first stay, make sure to:
                </p>
                <ul class="list-unstyled">
                    <li class="space-2">
                        <strong><a href="https://www.airbnb.com/users/edit">Complete Your Profile</a></strong>
                        <div>Upload a photo and write a short bio to help hosts get to know you before inviting you into their home.</div>
                    </li>
                    <li class="space-2">
                        <strong><a href="https://www.airbnb.com/verify">Provide ID</a></strong>
                        <div>Some hosts require guests to provide ID before booking. Get a head start by doing it now.</div>
                    </li>
                    <li class="space-2">
                        <strong><a href="https://www.airbnb.com/help/article/380">Learn How to Book a Place</a></strong>
                        <div>Get ready to search for the perfect place, contact hosts, and prepare for a memorable trip.</div>
                    </li>
                </ul>
            </div>    
        </div>
        <div class="card">
            <h4 class="jumbotron">Notifications</h4>
            <div class="card-block">
                <p class="card-text">
                    <ul class="list-group">
                        <li class="list-group-item" v-for="notification in notifications.notifications">
                            @{{ notification.data.message }}
                            <a @click="viewNotification(notification.id)" class="btn pull-right btn-sm btn-info">View</a>
                            <span></span>
                        </li>
                    </ul>
                </p>
            </div>
        </div>
    </div>
</div>
</section>     
@endsection

