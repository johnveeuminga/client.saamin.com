<header>
    <nav class="navbar navbar-toggleable-sm navbar-light bg-warning">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="{{ url('/') }}">SAAMIN</a>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link " href="{{ route('properties.index') }}">Properties</a>
                </li>
            </ul>

            <!-- Button trigger modal -->   
            <ul class="navbar-nav">
                @if(Auth::check())
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            Notifications<span class="tag tag-danger" v-if="notifications.unread_notifications"> @{{ countNotifications }}</span>
                        </a>
                        <div class="dropdown-menu" >
                            <p class="text-center">Latest Notifications</p>
                            <div class="dropdown-divider"></div>
                            <div v-for="notification in notifications.notifications">
                                <a class="dropdown-item" @click="viewNotification(notification.id)">@{{ notification.data.message }}</a>
                            </div>
                        </div>
                    </li>
                    <messages-header :id="id"></messages-header>
                @endif
                @if(Auth::guest())
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('register') }}">Sign Up</a>
                    </li>
                    <li class="nav-item">
                        <a href="/login" class="nav-link">Log In</a>
                    </li>
                @else
                    <li class="nav-item dropdown">  
                        <a class="nav-link dropdown-toggle userImg" role="button" data-toggle="dropdown">
                            <img src="{{ Auth::user()->avatar }}">{{Auth::user()->firstname}} {{Auth::user()->lastname}}
                        </a>
                        <div class="dropdown-menu ">
                            <a href="{{ url('/dashboard') }}" class="dropdown-item">My Dashboard</a>
                            @if(Auth::user()->userable_type == 'App\UserCustomer')
                                <a href="{{ url('/reservations') }}" class="dropdown-item">Reservations</a>
                            @else
                                <a href="{{ url('/properties?me') }}" class="dropdown-item">My Properties</a>
                            @endif
                            <div class="dropdown-divider"></div>                            
                            <a href="{{ url('/logout') }}"
                                onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();" class="dropdown-item text-xs-center"><i class="fa fa-sign-out" aria-hidden="true"></i>&nbsp;Log Out
                            </a>
                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </div>
                    </li>
                @endif
            </ul>
        </div>
    </nav>
</header>