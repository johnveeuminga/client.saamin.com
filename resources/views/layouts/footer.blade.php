<footer class="container-fluid pt-5 pb-5">
    <div class="row">
    	<div class="col pt-4 text-left">
    		<p class="fa fa-copyright" aria-hidden="true"></p> 
    		<script type="text/javascript">
    		var d = new Date()
    		document.write(d.getFullYear())
    		</script>|<a href="http://www.4ksoftware.io" class="text-danger" target="_blank">
    		FOUR K SOFTWARE</a> | ALL RIGHTS RESERVED 	
      	</div>
        <div class="footer-social col text-right">
            <ul class="social-network social-circle ">
                <li>
                    <a href="#" class="icoFacebook" title="Facebook"><i class="fa fa-facebook fa-lg"></i></a>
                </li>
                <li>
                    <a href="#" class="icoTwitter" title="Twitter"><i class="fa fa-twitter fa-lg"></i></a>
                </li>
                <li>
                    <a href="#" class="icoGoogle" title="Google +"><i class="fa fa-google-plus fa-lg"></i></a>
                </li>
            </ul>               
        </div>
    </div>
</footer>