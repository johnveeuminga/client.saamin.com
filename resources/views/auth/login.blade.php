@extends('layouts.master')

@section('content')
<section class="loginBg">
    <div class="container pt-3">
        <form class="form-login loginForm" role="form" method="POST" action="{{ url('/login') }}">
                {{ csrf_field() }}
            @include('components.success')
            <h2 class="form-signin-heading text-xs-center">Login</h2>
            <hr>
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="sr-only">E-Mail Address</label>
                <input id="email" type="email" class="form-control" name="email" placeholder="E-Mail Address" value="{{ old('email') }}" autofocus>
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </input>
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="sr-only">Password</label>
                <input id="password" type="password" class="form-control" name="password" placeholder="Password">
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
                </input>
            </div>
            <div class="form-group">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="remember"> Remember Me
                    </label>
                </div>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-outline-info">
                    Login
                </button>

                <a class="btn btn-outline-info" href="{{ url('/password/reset') }}">
                    Forgot Your Password?
                </a>
            </div>
            <div class="signup-or-separator">
                  <span class="h6 text-or-separator">or</span>
                  <hr>
            </div>
            <div class="social">
                <button class="social-login btn btn-outline-primary btn-block" href="/login/facebook">
                    <i class="fa fa-facebook-square fa-2x pull-left"></i>
                    <a class="login-text pt-1" href="/login/facebook">Log in with Facebook</a>
                </button>
                <button class="social-login btn btn-outline-secondary btn-block" >
                    <i class="fa fa-google fa-2x pull-left"></i>
                    <a class="login-text pt-1" href="/login/google">Log in with Google</a>
                </button>
            </div>
        </form>
    </div>
</section>
@endsection


