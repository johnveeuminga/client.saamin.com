@extends('layouts.master')

@section('content')
<section>
    <div class="registerBg">
        <div class="container pt-3 pb-3">
            <form class="form-login registerForm" role="form" method="POST" action="{{ url('/register') }}">
                    {{ csrf_field() }}

                <h2 class="form-signin-heading text-xs-center"> Register </h2>

                <hr>
                    <div class="form-group{{ $errors->has('firstname') ? ' has-error' : '' }}"  >
                        <label for="firstname" class="sr-only firstname" >Firstname</label>

                        <div>
                            <input id="firstname" type="text" class="form-control" name="firstname" placeholder="First Name" value="{{ old('firstname') }}" autofocus>

                            @if ($errors->has('firstname'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('firstname') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('middlename') ? ' has-error' : '' }}"  >
                        <label for="middlename" class="sr-only middlename" >Middle Name</label>

                        <div>
                            <input id="middlename" type="text" class="form-control" name="middlename" placeholder="Middle Name" value="{{ old('middlename') }}" autofocus>

                            @if ($errors->has('middlename'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('middlename') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }}"  >
                        <label for="lastname" class="sr-only lastname" >Last Name</label>

                        <div>
                            <input id="lastname" type="text" class="form-control" name="lastname" placeholder="Last Name" value="{{ old('lastname') }}"  autofocus>

                            @if ($errors->has('lastname'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('lastname') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="sr-only" >E-Mail Address</label>
                        <div>
                            <input id="email" type="email" class="form-control" name="email" placeholder="E-Mail Address" value="{{ old('email') }}">

                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="sr-only">Password</label>

                        <div>
                            <input id="password" type="password" class="form-control" name="password" placeholder="Password" >

                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        <label for="password-confirm" class="sr-only">Confirm Password</label>

                        <div>
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation"  placeholder="Confirm Password" >

                            @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <input type="hidden" name="usertype" value="App\UserCustomer" id="usertype">
                    <div class="form-group">
                        <div>
                            <button type="submit" class="btn btn-outline-info btn-block">
                                Register
                            </button>
                        </div>
                    </div>
                    <div class="signup-or-separator">
                      <span class="h6 text-or-separator">or</span>
                      <hr>
                    </div>
                    <div class="social">
                        <a href="{{ url('/login/facebook') }}" class="button social-login btn btn-outline-secondary btn-block">
                            <i class="fa fa-facebook-square fa-2x pull-left"></i>
                            Sign up with Facebook
                        </a>
                        <a href="{{ url('/login/google') }}" class="button social-login btn btn-outline-secondary btn-block">
                            <i class="fa fa-google-plus-square fa-2x pull-left"></i>
                            Sign up with Google
                        </a>
                    </div>
            </form>
        </div>
    </div>
</section>
@endsection
