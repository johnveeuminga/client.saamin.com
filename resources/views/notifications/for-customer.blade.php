@extends('layouts.master')
@section('content')
	<br>
	<div class="container">
		<div class="col-lg-4">
			<div class="card mx-auto text-xs-center">
				<br>
				<h4>Host Details</h4>
				@foreach($host->agent->users as $agent)
			  		<img class="card-img-top " src="{{ $agent->avatar }}" alt="Card image cap">
		  		    <p class="h6">
				        {{$agent->firstname }} {{ $agent->lastname}} 
				    </p>
				    <p class="small text-muted">Address - {{ $agent->address }}</p>
			  	@endforeach
				<hr>
			  	<div class="card-block">
			    	<h5>Trip Details</h5>
			    	<h5 class="h4">{{$property->name}}</h5>
				    <p class="text-muted">Address: {{$property->address}}</p>
				    <p class="small text-muted">{{$property->description}}</p>
				    <p class="small text-muted">
				        <i class="fa fa-credit-card fa-2x" aria-hidden="true"></i> 
				         Price: PHP  {{$property->price}}
				    </p>
				        <p class="small text-muted">
				        <i class="fa fa-cubes fa-2x" aria-hidden="true"></i> 
				         Bathrooms:  {{$property->bathrooms}}
				    </p>
				    <p class="small text-muted">
				        <i class="fa fa-bed fa-2x" aria-hidden="true"></i> 
				         Bedrooms:  {{$property->bedrooms}}
				    </p>
			    	<hr>
			    	<p>Checkin : {{ $property->pivot->checkin }}</p>
			    	<p>Checkout : {{ $property->pivot->checkout }}</p>
			    	<hr>
			    	<h5>Payment</h5>
			    	<p>Price : PHP {{ $property->price }}</p>
			    	<p>City Services : {{$property->city_services}}</p>
					<p><strong>Total Price : PHP {{ $property->price + $property->city_services  }}</strong></p>
			  	</div>
			</div>
		</div>
		<!-- end of left column -->
		<div class="col-lg-8">
			<div class="card">
				<div class="card-block">
					<h5 class="card-title">You message {{ $property->name }} about their listing</h5>
					<p class="card-text">Most host respond 48 hours. If unconfirmed within 48 hours, kindly message your host for confirmation.</p>
					@foreach($host->agent->users as $agent)
						<a href="{{ route('messages.show', $agent->id) }}" class="btn btn-sm btn-info">Message Host</a>
					@endforeach
				</div>
			</div>
		</div>
	</div>
@endsection