@extends('layouts.master')
@section('content')
	<br>
	<div class="container">
		<div class="col-lg-4">
			<div class="card mx-auto text-xs-center">
				<br>
			  	<img class="card-img-top " src="../img/founder.png" alt="Card image cap">
			  	<div class="card-block">
				    <h4 class="card-title">{{ $user->firstname }} {{ $user->lastname }}</h4>
				    <p class="card-text">{{ $user->address }}</p>
			  	</div>
			  	<div class="card-block">
			    	<h5>Trip Details</h5>
			    	<h6>{{ $property->name }}</h6>
			    	<hr>
			    	<p>Checkin : {{ $property->pivot->checkin }}</p>
			    	<p>Checkout : {{ $property->pivot->checkout }}</p>
			    	<hr>
			    	<p>Guests : Fix later</p>
			    	<h5>Payment</h5>
			    	<p>Price : {{ $property->price }}</p>
			    	<p>City Services : {{$property->city_services}}</p>
					<p>Total Price : {{ $property->price + $property->city_services  }}</p>
			  	</div>
			</div>
		</div>
		<!-- end of left column -->
		<div class="col-lg-8">
			<div class="card" v-show="!confirmed">
				<div class="card-block">
					<h5 class="card-title">Reservation Confirmation</h5>
					<p class="card-text">If unconfirmed within 48 hours. Reservation will be lost and customer needs to reserve your property again.</p>
					@if($property->pivot->status != 1)
						<button  @click="confirmReservation('{{$user->id}}', '{{$property->id}}')" class="btn btn-md btn-info">Confirm Reservation</button>
					@endif
				</div>
			</div>
			<div class="card">
				<div class="card-block ">
					<h5 class="card-title text-xs-center">Inquiry</h5>
					<ul class="list-group">
						<li class="list-group-item" style="height: 100px">
							{{ $inquiry}}
							<img class="pull-right" src="../img/founder.png" style="width: 50px;">

						</li>
						<br>
						<a href="{{ route('messages.show', $user->id) }}"class="btn btn-sm btn-info">Send Message</a>	
					</ul>
					<ul class="list-group" v-if="messages.length">
						<li class="list-group-item" style="height: 100px" v-for="message in messages">
							<img class="pull-right" src="../img/founder.png" style="width: 50px;">
							@{{ message.message }}
						</li>
					</ul>	
				</div>
			</div>
		</div>
	</div>
@endsection