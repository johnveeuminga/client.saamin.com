@extends('layouts.master')
@section('content')
	<br>
	<div class="container">
		<div class="col-md-8 offset-md-2">
			<div class="card chat">
				<div class="card-header mb-3">
					<img v-if="messages.user" :src="messages.user.avatar" alt="avatar">
					<span v-if="messages.user">Chat with @{{ messages.user.firstname }} @{{ messages.user.lastname }}</span>
				</div>
					@include('messages.conversations')
				<div class="chat-message clearfix">
					<textarea v-model="message" class="form-control" rows="3"></textarea>
					<input type="hidden" v-model="id" value="{{ $routeId }}">
					<button @click="submitMessage()" type="button" class="btn btn-info">Send Message</button>
				</div>
			</div>
		</div>
	</div>

@endsection