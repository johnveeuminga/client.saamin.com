@foreach($inboxes as $inbox)
	@if(!is_null($inbox->thread))
		<ul class="list-group">
			<li class="list-group-item">
				<img src="{{ $inbox->withUser->avatar }}" alt="avatar">
				<span>
					{{ $inbox->withUser->firstname }} {{ $inbox->withUser->lastname}}
				</span>
				<a href="{{ route('messages.show', ['id' => $inbox->withUser->id]) }}" class="pull-right btn btn-sm btn-success">Send Message</a>
				<div>
					@if(Auth::user()->id == $inbox->thread->sender->id)
						<span class="fa fa-reply"></span>
					@endif
					<span>{{ substr($inbox->thread->message, 0, 20) }}...</span>
					<p>{{ $inbox->thread->humans_time }} ago</p>
				</div>
			</li>
		</ul>
	@endif
@endforeach