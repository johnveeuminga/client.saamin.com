@extends('layouts.master')
@section('content')
	<br>
	<div class="container" id="app">
		<div class="card">
			<div class="card-header">
				Messages
			</div>
			<div class="card-block">
				@include('messages.inbox')
			</div>
		</div>
	</div>
@endsection