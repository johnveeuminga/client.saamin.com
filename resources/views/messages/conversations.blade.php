<div class="chat-history">
	<ul>
		<div v-for="message in messages.messages">
			<li v-if="message.sender.id == '{{ Auth::user()->id }}' " class="clearfix" style="list-style-type: none;">
				<div class="message-data align-right">
					<span class="message-data-time">@{{ message.humans_time }} ago</span> &nbsp; &nbsp;
					<span class="message-data-name">@{{ message.sender.firstname }} @{{ message.sender.lastname }}</span>
					<a href="" class="talkDeleteMessage" title="Delete Message"><i class="fa fa-close"></i></a>
				</div>
				<div class="message other-message float-right">
					@{{ message.message }}
				</div>
			</li>
			<li v-else style="list-style-type: none;">
				<div class="message-data">
					<span>@{{ message.sender.firstname }} @{{ message.sender.lastname }}</span>
					<span><span class="message-data-time">@{{ message.humans_time }} ago</span></span>
				</div>
				<div class="message my-message">
					@{{ message.message }}
				</div>
			</li>
		</div>
	</ul>
</div>


