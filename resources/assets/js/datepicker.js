Vue.component('date-pick', require('./components/DatePickerTemplate.vue'));
Vue.component('messages-header', require('./components/MessageHeader.vue'));
window.toastr = require('toastr');

$(document).on('click', '[data-toggle="lightbox"]', function(event) {
    event.preventDefault();
    $(this).ekkoLightbox();
});
// Added Pusher logging
Pusher.log = function(msg) {
    console.log(msg);
};
new Vue({
	el: 'body',
    props: ['inboxes','id'],
    data: {
        guests: '',
        message: '',
        messages: [],
        owner: '',
        inbox: '',
        notification: '',
        notifications: [],
        confirmed: false
    },
	ready(){
        let self = this;
        let pusher = new Pusher('d7e7d0020692c21b1624', {
            authEndpoint: 'http://saamin.com/api/v1/pusher/auth',
            cluster: 'ap1',
            encrypted: true
        });
        if(typeof currentlyLogin != 'undefined'){
            let channel = pusher.subscribe('private-notification.'+currentlyLogin);
            channel.bind('event-notification', function(data){
                self.notifyUser(data.message);
                self.getNotifications();
            });
            let channelDatabase = pusher.subscribe('private-database-notification.'+currentlyLogin);
            channelDatabase.bind('event-database-notification', function(response){
                self.$set('notifications', response.data);
            });
            let channelMessage = pusher.subscribe('private-message.'+currentlyLogin);
            channelMessage.bind('event-private-message', function(response){
                self.messages.messages.push(response.data);
            });
            self.getNotifications();
            self.getMessages();
        }
    },
    computed: {
        countNotifications: function(){
            if(this.notifications.unread_notifications.length){
                return this.notifications.unread_notifications.length;
            }
        }
    },
    methods: {
        submitRequest(owner, checkin, checkout, customerId, propertyId){
            this.owner = owner;
            this.$http.post('http://saamin.com/api/v1/customers/' + customerId + '/properties', {
                owner: owner,
                checkin: checkin,
                checkout: checkout,
                propertyId: propertyId,
                currentlyLogin: currentlyLogin,
                message: this.message
            }).then((response) => {
                window.location.href="http://client.saamin.com/properties/"+propertyId;
            });
        },
        notifyUser: (message) => {
            toastr.success(message, null, {
                "positionClass": "toast-top-right"
            });
        },
        getNotifications(){
            this.$http.get('http://saamin.com/api/v1/users/'+currentlyLogin + '/inbox').then((response) => {
                this.notifications = response.body.data;
            });
        },
        viewNotification(id){
            window.location.href="http://client.saamin.com/notifications/"+id;
            this.$http.get('http://saamin.com/api/v1/users/'+currentlyLogin + '/inbox/'+id);
        },
        confirmReservation(customerId, propertyId){
            this.$http.post('http://saamin.com/api/v1/customers/'+customerId + '/properties/'+propertyId +'/confirm')
                .then((response) => {
                    this.confirmed = response.data
            });
        },
        submitMessage(){
            this.$http.post('http://saamin.com/api/v1/users/' + this.id+ '/messages', {
                message: this.message,
                currentlyLogin: currentlyLogin
            }).then((response) => {
                this.message = '';
                this.messages.messages.push(response.data.data);
            });
        },
        getMessages(){
            this.$http.get('http://saamin.com/api/v1/users/' + this.id + '/messages/null',{
                params: {currentlyLogin: currentlyLogin}
            }).then((response) => {;
                this.messages = response.body.data;
            });
        }
    }
})
