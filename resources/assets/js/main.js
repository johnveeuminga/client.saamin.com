require('./bootstrap');
require('./datepicker');

$( document ).ready(function() {
    var id;
    Dropzone.autoDiscover = false;
    $('#insertImageAdd').click(function(){
        console.log('clicked');
         $("<div class='form-group row'> <div class='col-sm-4 offset-sm-2'> <input type='file' class='form-control' id='image' placeholder='Image of property' name='image[]'></div></div>").appendTo('#addImage')
    });
    $('#insertImage').click(function(){
        console.log('clicked');
         $("<div class='form-group row'> <div class='col-sm-4 offset-sm-2'> <input type='file' class='form-control' id='image' placeholder='Image of property' name='image[]'></div></div>").appendTo('#addImage')
    });
    $('.deleteVideo').click(function(){
        console.log('clicked');
        id = ($(this).attr("value"));
        $('#deleteForm').attr("action", '/videos/'+id);
    });
    $('.deletePhoto').click(function(){
        id = ($(this).attr("value"));
        $('#deleteForm').attr("action", '/photos/'+id);
    });
});