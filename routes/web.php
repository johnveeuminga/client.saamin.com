<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index');
Route::get('/about', 'HomeController@about');
Route::get('/faq', 'HomeController@faq');
Route::post('/contactUs', 'HomeController@contactUs');

Route::get('/dashboard', 'DashboardController@dashboard');
Route::get('/agents/properties', 'UserAgentController@index');
Route::get('/reservations', 'UserCustomerController@index');
Route::resource('/properties', 'PropertyController');
Route::get('/properties/{id}/book', 'UserCustomerController@showBookPage')->name('properties.book');
Route::post('/properties/{id}/book', 'UserCustomerController@book');
Route::resource('properties.video', 'PropertyVideoController', ['only' => ['store', 'update', 'destroy']]);
Route::post('properties/{property}/photos/{photo}', 'PropertyPhotoController@update');
Route::resource('properties.photos', 'PropertyPhotoController',['only' => ['store', 'update', 'destroy']]);
Route::post('properties/{property}/video/{video}', 'PropertyVideoController@update');

Route::resource('messages', 'UserInboxController',['only' => ['show','index']]);
Route::resource('inbox', 'InboxController',['only' => 'index']);
Route::get('/activities', 'ActivityController@index');
Route::get('/notifications/{id}', 'NotificationController@show')->name('notifications.show');

Route::get('/login/{provider}', 'Auth\LoginController@redirectToProvider')->name('login.social');
Route::get('/login/{provider}/callback', 'Auth\LoginController@handleProviderCallback');

