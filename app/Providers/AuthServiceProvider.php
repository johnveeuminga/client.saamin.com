<?php

namespace App\Providers;

use App\Api\ApiUserProvider;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Auth::provider('api', function($app, array $config){
            
            return new ApiUserProvider(
                new User([]),

                new Client([
                       'base_uri' => config('api.base_uri') 
                ])
            );
        });

        Auth::provider('social-api', function(){

            return new SocialApiUserProvider(

                new User([]),

                new Client([

                    'base_uri' => config('api.base_uri')
                ])
            );
        });
    }
}
