<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ConfirmCustomer
{
    use InteractsWithSockets, SerializesModels;

    public $property;
    public $user;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($property, $user)
    {
        $this->property = $property;
        $this->user = $user;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('confirm-booking');
    }
}
