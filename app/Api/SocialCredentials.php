<?php

namespace App\Api;

use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;

trait SocialCredentials
{
    /**
     * 
     * First name of User from Social Provider
     * @var [type]
     */
    protected $firstname;

    /**
     * 
     * Middle name of User from Social Provider
     * @var string
     */
    protected $middlename = 'Optional';

    /**
     * 
     * Last name of User from Social Provider
     * @var string
     */
    protected $lastname;
    
	/**
     * Redirect to Social Provider website
     * 
     * @param  $provider 
     * @return Response
     */
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain information from Social Provider website
     * 
     * @param  $provider
     * @return Response
     */
    public function handleProviderCallback($provider)
    {
    	try{

    		$user = Socialite::driver($provider)->stateless()->user();

	        $authenticatedUser = $this->findOrCreateUser($user, $provider);

	        //Since the login method on UserCredentials accepts Illuminate Request.
	        //It is neccessary to create a new Request instance.        
	        //Value password will be used as a default password 
	        
	        $request = new Request([
	            'email' => $authenticatedUser->email,
	            'password' => 'password'
	        ]);

	        $this->login($request);

	        return redirect('/dashboard');
    	}catch(RequestException $e)
    	{
    		return redirect('/login');
    	}

    }

    /**
     * 
     * Will check if user has an existing account
     * If none will create new account
     * 
     * @param  Socialite $user    
     * @param  $provider 
     * @return User instance
     */
    public function findOrCreateUser($user, $provider)
    {
        try{

            $response = $this->client->get('api/v1/users/'.$user->id);

        }catch(RequestException $e){

            $response = $e->getResponse()->getBody();
            $error = json_decode($response->getContents())->error;

            if($error->http_code === 404)
            {
                $name = $this->extractName($user->name);

                $newUser = $this->createUser($user, $name, $provider);

                return json_decode($newUser->getBody())->data;

            }

        }

        return json_decode($response->getBody())->data;

    }

    /**
     * 
     * Will separate name into first,middle and last name
     * 
     * @return array
     */
    public function extractName($name)
    {
        $splitName = explode(' ', $name);

        $this->firstname = array_shift($splitName);

        if(sizeof($splitName) > 1)
        {
            $this->middlename = $splitName[0];

        }

        $this->lastname = array_values(array_slice($splitName, -1))[0];

        return [
            'firstname' => $this->firstname,
            'middlename' => $this->middlename,
            'lastname' => $this->lastname
        ];  
    }

    /**
     * Will push fields into Saamin Api
     * 
     * @return Response
     */
    public function createUser($user, $fullName, $provider)
    {
        return $this->client->post('api/v1/users', [

            'form_params' => [

                'email' => $user->email,
                'firstname' => $fullName['firstname'],
                'middlename' => $fullName['middlename'],
                'lastname' => $fullName['lastname'],
                'userable_id' => 30,
                'address'   => 'Provide address',
                'userable_type' => 'App\UserCustomer',
                'provider' => $provider,
                'provider_id' => $user->id,
                'password' => 'password',
                'avatar' => $user->avatar,
                'location' => 'Provide location'
            ]
        ]);
    }
}