<?php

namespace App\Api;

use App\User;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;

class UserFromApi
{
	/**
	 * App\User
	 * 
	 * @var object
	 */
	protected $user;

	/**
	 * The client used by GuzzleHttp
	 * 
	 * @var GuzzleHttp/Client
	 */
	protected $client;

	/**
	 * Attributes of user from Api
	 * 
	 * @var array
	 */
	protected $attributes = [];

	/**
	 * Create new User Instance
	 * 
	 * @param User   $user   
	 * @param Client $client 
	 */
	public function __construct(User $user, Client $client)
	{
		$this->user = $user;
		$this->client = $client;
	}

	/**
	 * Check if passed field matches user attributes from Api
	 * 
	 * @param  mixed $field 
	 * @return User|null
	 */
	public function queryApiUserBy($field)
	{
		$attributes = $this->getApiUser();

		foreach($attributes as $attribute){

			if($attribute == $field){

				$this->attributes = $attributes;

				break;
			}			
		}

		return $this->attributes ?: null;
	}

	/**
	 * Receive User from Api
	 * 
	 * @return array
	 */
	protected function getApiUser()
	{
		try {
			$response = $this->getAccessTokenFromApi();
			$user = $this->client->request('GET', '/api/user',[
				'headers' => [
					'Content-Type' => 'application/json',
					'Authorization' => 'Bearer '.$response['access_token']
				]
			]);
		} catch (ClientException $e) {
			return $e->getResponse();
		}
		
		return json_decode($user->getBody(), true);
	}

	/**
	 * Get access token from Api Laravel Passport
	 * 
	 * @return array/access_token
	 */
	protected function getAccessTokenFromApi()
	{
		$token = $this->client->post('/oauth/token',[

			'form_params' => [

				'client_id'		=> config('services.passport.client_id'),
				'client_secret' => config('services.passport.client_secret'),
				'grant_type' 	=> config('services.passport.grant_type'),
				'username'  	=> session('username'),
				'password' 		=> session('password'),
				'scope' 		=> '*'
			]
		]);

		return json_decode($token->getBody(), true);
	}

	/**
	 * create new User instance
	 * 
	 * @param  array $attributes 
	 * @return new User
	 */
	protected function createNewUser($attributes)
    {
    	if(!is_null($attributes)) return new User($attributes);
    }
}