<?php

namespace App\Api;

use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RedirectsUsers;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

trait RegistrationCredentials
{
    use RedirectsUsers;

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        return view('auth.register');
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {      
        $token = $this->getAccessTokenFromApi();

        $this->sendHeader($token);

        try{

            $user = $this->create($request->all());

        }catch(RequestException $e){
            
            $error = $e->getResponse()->getBody();
            $message = json_decode($error->getContents(), true);

            return back()->withErrors($message['error']['message'])->withInput($request->except('password'));

        }
        
        $request->session()->put(['username' => $request->email,'password' => $request->password]);

        if(Auth::attempt(['username' => $request->email, 'password' => $request->password])){

            return redirect($this->redirectPath());        
        }
        
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }
}