<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Input;

class PropertyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {   

         $rules =   [
            'address' => 'required',
            'name' => 'required',
            'description' => 'required',
            'floor_size' => 'required|numeric',
            'price'=> 'required|numeric',
            'bathrooms' => 'required|numeric',
            'bedrooms' => 'required|numeric',
            'latitude' =>  'required|numeric',
            'longitude' => 'required|numeric',
            'city_services' => 'required|numeric'
        ];

        return $rules;

    }
    /**
     * Return errors when validation failed
     * 
     * @param  array  $errors 
     * @return Response
     */

    public function response(array $errors)
    {
        return json_decode(response()->json([
            'error' => [
                'message' => $errors
            ]
        ], 422));

    }
}
