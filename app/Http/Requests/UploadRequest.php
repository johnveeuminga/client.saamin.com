<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UploadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =[];
        if($request->hasFile('image')){
            foreach ($request->image as $key => $value) {
                $rules['image.'.$key]='required|image';
            }
        }
        if($request->hasFile('imageEdit')){
            foreach($request->imageEdit as $key => $value){
                $rules['imageEdit.'.$key]='required|image';
            }
        }
        if($request->hasFile('video')){
            foreach ($request->video as $key => $value) {
                # code...
                $rules['video.'.$key]='required|required|mimetypes:video/avi,video/mpeg,video/quicktime';
            }
        }
        if($request->hasFile('videoEdit')){
            foreach ($request->video as $key => $value) {
                $rules['video.'.$key]='required|required|mimetypes:video/avi,video/mpeg,video/quicktime';
            }
        }

        return $rules;
    }
}
