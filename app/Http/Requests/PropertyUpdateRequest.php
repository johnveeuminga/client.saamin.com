<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use Illuminate\Support\Facades\Input;

class PropertyUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'address' => 'required',
            'name' => 'required',
            'description' => 'required',
            'floor_size' => 'required|numeric',
            'price'=> 'required|numeric',
            'bathrooms' => 'required|numeric',
            'bedrooms' => 'required|numeric',
            'latitude' =>  'required|numeric',
            'longitude' => 'required|numeric',
            // 'image'   => 'image',
            // 'video' => 'mimetypes:video/avi,video/mpeg,video/quicktime,video/mp4',
            // 'imageEdit' => 'image',
            // 'videoEdit' => 'mimetypes:video/avi,video/mpeg,video/quicktime,video/mp4',
        ];
    }

    public function messages(){
        return [
            'image.image' => "There was a problem in uploading your photos. Please check the file format of your images.",
            'video.mimetypes' => "There was a problem in uploading your video. Please check the file format of your videos.",
            'imageEdit.image' => "There was a problem in uploading your photos. Please check the file format of your images.",
            'videoEdit.mimetypes' => "There was a probleim in uploading your video. Please check the file format of your videos."

        ];
    }
}
