<?php

namespace App\Http\Controllers;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends ClientController
{
    /**
     * Will instantiate HomeController 
     *
     * @param ClientInterface $client 
     */
    public function __construct(ClientInterface $client)
    {
        parent::__construct($client);
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $props = $this->performRequest('GET', 'api/v1/properties/featured')->data;
        // dd($props);
        return view('welcome', compact('props'));
    }

    public function about()
    {
        return view('about');
    }

    public function faq()
    {
        return view('faq');
    }
    /**
     * Will send data first to saamin api
     * 
     * @return Response
     */
    public function contactUs(Request $request)
    {
        $response = $this->contactUsApi($request);
        if(is_null($response))
        {
            $this->contactUsApi($request);
        }
        return redirect()->back()->with('success', $response->data->message);
    }

}
