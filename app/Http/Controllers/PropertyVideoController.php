<?php

namespace App\Http\Controllers;

use GuzzleHttp\ClientInterface;
use Illuminate\Http\Request;

class PropertyVideoController extends ClientController
{
    /**
     * Will create new PropertyVideoController
     * 
     * @param ClientInterface $client [description]
     */
    public function __construct(ClientInterface $client)
    {
        parent::__construct($client);
        $this->middleware('auth');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        if($request->hasFile('video')){
            $this->uploadVideo($request->video, $id,  $request->propertyName);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $propertyId, $videoId)
    {
        if($request->hasFile('video')){
            $video = $this->editVideo($request->video, $videoId, $propertyId, $request->propertyName);
        }
        return ($video);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($propertyId, $videoId)
    {
        $video = $this->deleteVideo($videoId);
        $property = $this->showOneProperty($propertyId);
        return redirect("/properties/{$property->data->slug}/")
            ->with([
                'property', $property->data,
                'success', $video->message
            ]);
    }
}
