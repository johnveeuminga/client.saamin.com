<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;

class UserAgentController extends ClientController
{
	/**
	 * Create new Controller instance.
	 *
	 * @return  void 
	 */
	public function __construct()
	{
		$this->middleware(['auth','agent']);
	}

	/**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$properties = $this->displayProperties()->data;
    	$userAgentId = Auth::user()->userable_id;
    	return view('agents.properties', compact('properties','userAgentId'));

    }

}
