<?php

namespace App\Http\Controllers\Auth;

use App\Api\SocialCredentials;
use App\Api\UserCredentials;
use App\Http\Controllers\ClientController;
use GuzzleHttp\ClientInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends ClientController
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use UserCredentials;
    use SocialCredentials;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ClientInterface $client)
    {
        parent::__construct($client);
        $this->middleware('guest', ['except' => 'logout']);
    }


}
