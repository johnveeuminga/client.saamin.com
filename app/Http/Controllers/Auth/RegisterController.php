<?php

namespace App\Http\Controllers\Auth;

use App\Api\RegistrationCredentials;
use App\Http\Controllers\ClientController;
use Auth;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Foundation\Auth\RedirectsUsers;
use Illuminate\Http\Request;
use Validator;

class RegisterController extends ClientController
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    //use RegistrationCredentials;
    use RedirectsUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Instance of RegisterController
     * 
     * @return  void 
     */
    public function __construct(ClientInterface $client)
    {
        parent::__construct($client);
        $this->middleware('guest');
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        return view('auth.register');
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {     
        $token = $this->getAccessTokenFromApi();
        try{
            $user = $this->create($request->all(), $token);
        }catch(RequestException $e){ 
            $error = $e->getResponse()->getBody();
            $message = json_decode($error->getContents(), true);
            return back()->withErrors($message['error']['message'])->withInput($request->except('password'));
        }
        
        $request->session()->put(['username' => $request->email,'password' => $request->password]);

        if(Auth::attempt(['username' => $request->email, 'password' => $request->password])){

            return redirect($this->redirectPath());        
        }
        
    }

    /**
     * Will create new user
     * 
     * @param  array  $data 
     * @param  array  $token 
     * @return array
     */
    protected function create(array $data, array $token)
    {
        $response = $this->client->post('/api/register',[
            'json' => [
                    'email' => $data['email'],
                    'password' => $data['password'],
                    'password_confirmation' => $data['password_confirmation'],
                    'firstname' => $data['firstname'],
                    'middlename' => $data['middlename'],
                    'lastname' => $data['lastname'],
                    'userable_type' => $data['usertype']
            ],
            'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer '.$token['access_token']
            ]        
        ]);
        return json_decode($response->getBody(), true);
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }


}
