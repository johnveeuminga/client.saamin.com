<?php

namespace App\Http\Controllers;

use GuzzleHttp\ClientInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NotificationController extends ClientController
{
    /**
     * Instantiate Notificationt
     * 
     * @param ClientInterface $client
     */
    public function __construct(ClientInterface $client)
    {
        parent::__construct($client);
        $this->middleware('auth');
    }
    /**
     * Show one Notification of User
     * 
     * @param  int $id 
     * @return View
     */
    public function show($id)
    {
    	$userId = Auth::user()->id;
    	$response = $this->getUserInbox($userId, $id);
    	$notification = json_decode($response->getBody());
        
        $propertySlug = $notification->data->property;
        if(Auth::user()->userable_type != 'App\UserCustomer')
        {
            $customerId = $notification->data->customer_id;
            $customer = $this->getUserNotifications($customerId);
            $property = $this->getCustomerProperty($customerId, $propertySlug);

            return view('notifications.for-agent')->with([
                'user' => json_decode($customer->getBody())->data,
                'property' => json_decode($property->getBody())->data,
                'inquiry' => $notification->data->inquiry
            ]);
        }
        $property = $this->getCustomerProperty($userId, $propertySlug);
        $host = $this->showOneProperty($propertySlug);
        return view('notifications.for-customer')->with([
            'property' => json_decode($property->getBody())->data,
            'host' => $host->data
        ]);
    }
}
