<?php

namespace App\Http\Controllers;

use GuzzleHttp\ClientInterface;
use Illuminate\Http\Request;
use Auth;

class InboxController extends ClientController
{
    /**
     * Create new Controller instance.
     *
     * @return  void 
     */
    public function __construct(ClientInterface $client)
    {
        parent::__construct($client);
        $this->middleware(['auth']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $response = $this->getAllUserInbox(Auth::user()->id);
        $notifications = json_decode($response->getBody());
        return view('messages.index')->with('notifications', $notifications);
    }
}
