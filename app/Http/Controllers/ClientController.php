<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Auth;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    /**
     * GuzzleHttp\Client
     * 
     * @var object
     */
    protected $client;

    /**
     * Create new Client Instance
     * 
     * @param GuzzleHttp\Client $client 
     */
    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * Get accesstoken from Api
     * 
     * @return array AccessToken
     */
    public function getAccessTokenFromApi()
    {
        $response = $this->client->request('POST','/oauth/token',[

            'form_params' => [

                'client_id'     => config('services.passport.client_id'),
                'client_secret' => config('services.passport.client_secret'),
                'grant_type'    => 'client_credentials',
                'scope'         => '*'
            ]
        ]);

        return json_decode($response->getBody(), true);
    }

    /**
     * Main method for performing connection to saamin api
     * 
     * @param  string $method     
     * @param  string $url        
     * @param  array  $parameters
     * @return Guzzle\Response
     */
    protected function performRequest($method, $url, $parameters=[])
    {
        $response = $this->client->request($method, $url, $parameters);
        return json_decode($response->getBody());
    }

    /**
     * Show all properties
     * 
     * @return [Response
     */
    protected function displayProperties($userId = null)
    {
        return $this->performRequest('GET','/api/v1/properties/',[
            'query' => [
                'userId' => $userId
            ]
        ]);
    }

    /**
     * Display single property
     * 
     * @param  int $id 
     * @return Response
     */
    protected function showOneProperty($id)
    {
        return $this->performRequest("GET", "/api/v1/properties/{$id}");
    }

    /**
     * Create a single property
     * 
     * @param  array 
     * @return Response
     */
    protected function createProperty($parameters)
    {
         try{
            $response =  $this->performRequest("POST", "api/v1/properties/", [
            'form_params' => [
                'name'=>$parameters['name'],
                'address'=>$parameters['address'],
                'description'=>$parameters['description'],
                'size'=>$parameters['floor_size'],
                'price'=>$parameters['price'],
                'city_services' => $parameters['city_services'],
                'bedrooms'=>$parameters['bedrooms'],
                'bathrooms'=>$parameters['bathrooms'],
                'user_agent_id'=>Auth::user()->userable_id, 
                'lat' => $parameters['latitude'], 
                'lng'=> $parameters['longitude'],
                'slug' => str_slug($parameters['name']),
                'zoom' => $parameters['zoom']]]);
        }catch(RequestException $error){
            $response = $error->getResponse();
            return json_decode($response->getBody()->getContents());
        }
        return $response;
    }
    /**
     * Update a single property
     * 
     * @param  array $parameters 
     * @param  int $id         
     * @return Response
     */
    protected function updateProperty(array $parameters, $id)
    {
        try{
            $response =  $this->performRequest("PUT","/api/v1/properties/{$id}", [
            'form_params' => [
                'name'=>$parameters['name'],
                'address'=>$parameters['address'],
                'description'=>$parameters['description'],
                'size'=>$parameters['floor_size'],
                'price'=>$parameters['price'],
                'city_services' => $parameters['city_services'],
                'bedrooms'=>$parameters['bedrooms'],
                'bathrooms'=>$parameters['bathrooms'],
                'user_agent_id'=>Auth::user()->userable_id, 
                'lat' => $parameters['latitude'], 
                'lng'=> $parameters['longitude'],
                'zoom'=> $parameters['zoom']
                ]
            ]);
        }catch(RequestException $error){
            $response = $error->getResponse();

            return json_decode($response->getBody()->getContents());
        }

        return $response;
    }
    /**
     * Will delete Property
     * 
     * @param  int $id 
     * @return Response  
     */
    protected function deleteProperty($id)
    {
        return $this->performRequest("DELETE", "/api/v1/properties/{$id}");
    }

    /**
     * Will upload photo to saamin storage
     * 
     * @param  file $photo        
     * @param  int $propertyId   
     * @param  string $propertyName 
     * @return Response
     */
    protected function uploadPhoto($photo, $propertyId, $propertyName)
    {
        try{
            $response = $this->performRequest('POST', '/api/v1/photos/', [
                'multipart' => [
                    [
                        'name'  => 'propertyId',
                        'contents' => $propertyId
                    ],
                    [
                        'name' => 'propertyName',
                        'contents' => $propertyName
                    ], 
                    [
                        'name' => 'image',
                        'contents'=> fopen($photo, 'r')
                    ],
                    [
                        'name' => 'extension',
                        'contents' => $photo->getClientOriginalExtension()
                    ]  
                ]
            ]);   
         }catch(RequestException $error){
            $response = $error->getResponse();
            return json_decode($response->getBody()->getContents());
         }  
        return $response;
    }
    /**
     * Will edit photo
     * 
     * @param  file $photo       
     * @param  int $photoId  
     * @param  int $propertyId   
     * @param  string $propertyName 
     * @return Response
     */
    protected function editPhoto($photo, $photoId, $propertyId, $propertyName)
    {
        $this->performRequest('POST', "/api/v1/photos/{$photoId}", [
            'multipart' => [
                [
                    'name'=> 'propertyId',
                    'contents' => $propertyId
                ],
                [
                    'name' => 'propertyName',
                    'contents' => $propertyName
                ],
                [
                    'name'=>'image',
                    'contents'=>fopen($photo, 'r')
                ],
                [
                    'name' => 'extension',
                    'contents' => $photo->getClientOriginalExtension()
                ]
            ]
        ]);   
    }
    /**
     * Will delete photo
     * 
     * @param  int $id 
     * @return Response
     */
    protected function deletePhoto($id){
        return $this->performRequest('DELETE', "/api/v1/photos/{$id}");
    }
    /**
     * Will upload new video to saamin storage
     * 
     * @param  file $video       
     * @param  int $propertyId  
     * @param  string $propertyName 
     * @return Response
     */
    protected function uploadVideo($video, $propertyId, $propertyName)
    {
        $response=false;
        try{
            $reponse = $this->performRequest('POST', '/api/v1/videos/', [
                'multipart' => [
                    [
                        'name' => 'propertyId',
                        'contents' => $propertyId

                    ],
                    [
                        'name' => 'propertyName',
                        'contents' => $propertyName
                    ],
                    [
                        'name' => 'video',
                        'contents' => fopen($video, 'r')
                    ],
                    [
                        'name' => 'extension',
                        'contents' => $video->getClientOriginalExtension()
                    ]

                ]

            ]);
        }catch(RequestException $error){
            $response = $error->getResponse();

            return json_decode($response->getBody()->getContents());
        }
        return $response;   
    }
    /**
     * Will edit video 
     * 
     * @param  file $video        
     * @param  int $videoId      
     * @param  int $propertyId   
     * @param  string $propertyName 
     * @return Response              
     */
    protected function editVideo($video, $videoId, $propertyId, $propertyName)
    {
        $this->performRequest('POST', "/api/v1/videos/{$videoId}", [
            'multipart' => [
                [
                    'name'=> 'propertyId',
                    'contents' => $propertyId

                ],
                [

                    'name'=>'propertyName',
                    'contents' => $propertyName
                ],
                [
                    'name'=>'video',
                    'contents'=>fopen($video, 'r')

                ],
                [
                    'name' => 'extension',
                    'contents' => $video->getClientOriginalExtension()

                ]
            ]
        ]);   
    }
    /**
     * Will delete single video
     * 
     * @param  int $id 
     * @return Response     
     */
    protected function deleteVideo($id)
    {
        return $this->performRequest('DELETE', "/api/v1/videos/{$id}");
    }

    /**
     * Display all properties for user
     * 
     * @param  int $id 
     * @return Response
     */
    protected function customerProperties($id)
    {
        return $this->performRequest("GET", "/api/v1/customers/{$id}/properties");
    }

    /**
     * Send message to saamin api
     * 
     * @param  Request $request 
     * @return Response
     */
    protected function sendMessageToHost(Request $request)
    {
        return $this->performRequest("POST", "api/v1/messages",[

            'form_params' => [
                'message' => $request->message,
                'recipient' => $request->recipient,
                'user_id' => Auth::user()->id
            ]
        ]);
    }
    /**
     * User can book property
     * 
     * @param  Request $request 
     * @param  int  $id      
     * @return Response
     */
    protected function bookProperty(Request $request, $id)
    {
        $user = Auth::user()->id;
        try{
            $response = $this->client->post('/api/v1/customers/' .$user.'/properties', [
                'json' => [
                    'owner' =>  $request->old('recipient'),
                    'checkin' => $request->old('checkin'),
                    'checkout' => $request->old('checkout'),
                    'propertyId' =>  $id,
                    'currentlyLogin' => $user,
                    'message' => $request->message
                ]
            ]); 
        }catch(RequestException $error){
            $response = $error->getResponse();
            return json_decode($response->getBody()->getContents());
        }
        return json_decode($response->getBody());
    }
    /**
     * Will get inbox of user
     * 
     * @param  int $userId 
     * @param  int $id     
     * @return Response       
     */
    public function getUserInbox($userId, $id)
    {
        return $this->client->get("/api/v1/users/{$userId}/inbox/{$id}");
    }
    public function getAllUserInbox()
    {
        return $this->client->get("api/v1/users/".Auth::user()->id."/inbox");
    }
    /**
     * Will get User notification on saamin database
     * 
     * @param  int $userId 
     * @return Response        
     */
    public function getUserNotifications($userId)
    {
        return $this->client->get("/api/v1/users/{$userId}");
    }
    /**
     * Will fetch customers property
     * 
     * @param  id $customerId 
     * @param  mixed $property   
     * @return Response           
     */
    public function getCustomerProperty($customerId, $property)
    {
        return $this->client->get("/api/v1/customers/{$customerId}/properties/{$property}");
    }
    public function contactUsApi(Request $request)
    {
        try{
            $response = $this->performRequest('POST','/api/v1/contactUs',[
                'form_params' => [
                    'name' => $request->name,
                    'email' => $request->email,
                    'message' => $request->message
                ]
            ]);
        }catch(RequestException $error){
            $response = $error->getResponse();
            return json_decode($response->getBody()->getContents());
        }
        return $response;
    }
}
