<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class DashboardController extends Controller
{

	/**
	 * User must log in to access dashboard
	 * 
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Will redirect user to dashboard depends on user type
	 * 
	 * @var Illuminate/View
	 */
	public function dashboard(Request $request)
	{
		$user = $request->user();
		if(Auth::check()){
			if($user->userable_type == 'App\UserAdmin'){
	        	return view('dashboard.admin');
		    }elseif($user->userable_type == 'App\UserAgent') {
		        return view('/dashboard.agent');
		    }
		    return view('dashboard.customer');
		}
	}    
}
