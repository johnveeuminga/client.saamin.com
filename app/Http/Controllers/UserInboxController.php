<?php

namespace App\Http\Controllers;

use Auth;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;

class UserInboxController extends ClientController
{
    /**
     * Create new UserInboxController
     * 
     * @param ClientInterface $client [description]
     */
    public function __construct(ClientInterface $client)
    {
        parent::__construct($client);
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Auth::user()->id;
        $response = $this->client->get("/api/v1/users/{$id}/messages");
        $inboxes = json_decode($response->getBody());

        return view('messages.index')->with([
            'inboxes' => $inboxes->data
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {  
        return view('messages.show')->with([
            'routeId' => $id
        ]);
    }
}
