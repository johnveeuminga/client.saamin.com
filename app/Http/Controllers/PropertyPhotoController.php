<?php

namespace App\Http\Controllers;

use App\Photo;
use GuzzleHttp\ClientInterface;
use Illuminate\Http\Request;

class PropertyPhotoController extends ClientController
{
    public function __construct(ClientInterface $client)
    {
        parent::__construct($client);
        $this->middleware('auth');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $propertyId)
    {
        if($request->hasFile('photo')){
            foreach ($request->photo as $photo){
                $this->uploadPhoto($photo, $propertyId, $request->propertyName);
            }
        }else{
            return ('false');
        }
        return($photo);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $propretyId, $photoId)
    {
        if($request->hasFile('photo')){
           $photo =  $this->editPhoto($request->photo, $photoId, $propretyId, $request->propertyName);
        }  
        return($photo);  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($propertyId, $photoId)
    {
        $photo = $this->deletePhoto($photoId);
        $property = $this->showOneProperty($propertyId);
        return redirect("/properties/{$property->data->slug}/")
            ->with([
                'property', $property->data,
                'success', $photo->message
            ]);
    }
}
