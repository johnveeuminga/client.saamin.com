<?php
namespace App\Http\Controllers;

use App\Http\Controllers\ClientController;
use App\Http\Requests;
use App\Http\Requests\BookRequest;
use App\User;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UserCustomerController extends ClientController
{
    /**
	 * Create new Controller instance.
	 *
	 * @return  void 
	 */
	public function __construct(ClientInterface $client)
    {
		parent::__construct($client);
		$this->middleware(['auth','customer']);
	}
	/**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$properties = $this->customerProperties(Auth::user()->id);
    	return view('customers.reservations')->with('properties', $properties->data);
    }
    /**
     * Pre book Property page with property information
     * 
     * @param  Request $request 
     * @param  int  $id     
     * @return Response
     */
    public function showBookPage(BookRequest $request, $id)
    {
        $request->flash();
        $property = $this->showOneProperty($id);
        $checkin = $request->checkin;
        $checkout = $request->checkout;
        $recipient = $request->recipient;
        return view('properties.book')->with([
            'property' => $property->data, 
            'checkin' => $checkin, 
            'checkout' => $checkout,
            'recipient' => $recipient
        ]);
    }
    public function book(Request $request, $id)
    {
        $response = $this->bookProperty($request, $id);
        if(isset($response->error)){
            return redirect("/properties/{$id}")->with('errors', 'Unable to process request');
        }
  
        return redirect("/properties/{$id}")->with('success', $response->data->message);
    }
}
