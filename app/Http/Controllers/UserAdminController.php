<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class UserAdminController extends Controller
{
	/**
	 * Create new Controller instance.
	 *
	 * @return  void 
	 */
	public function __construct()
	{
		$this->middleware(['auth','admin']);
	}

	/**
	 * Show the application dashboard.
	 * 
	 * @return Illuminat\Http\Response
	 */
    public function index()
    {
    	return view('dashboard.admin');
    }
}
