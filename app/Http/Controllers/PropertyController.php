<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\PropertyRequest;
use Auth;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;
use Validator;

class PropertyController extends ClientController
{
    /**
     * @var boolean
     */
    protected $propertyAlreadyBooked = false;
    /**
     * Create new PropertyController instance
     *
     * @return void 
     */
    public function __construct(ClientInterface $client)
    {
        parent::__construct($client);
        $this->middleware(['auth','agent'], ['except' => ['show', 'showBookPage','book', 'index'] ]);
    }

    /**
     * Display all properties
     * 
     * @return Response
     */
    public function index(Request $request)
    {
        if(!is_null($request->get('me')))
        {
            $me = Auth::user()->id;
            $properties = $this->displayProperties($me);

            return view('agents.properties')->with('properties', $properties->data);
        }
    	$properties = $this->displayProperties();

    	return view('properties.index')->with('properties', $properties->data);
    }

    /**
     * Property Form for adding new property
     * 
     * @return Response
     */
    public function create()
    {
    	return view('properties.create');
    }

    /**
     * Send fields to saamin api for storing data in database
     * 
     * @param  PropertyRequest $request 
     * @return Response
     */
    public function store(PropertyRequest $request)
    {
    	$newProperty = $this->createProperty($request->all());
        if($newProperty == null){
            return redirect()->back()
                ->withInput()
                ->withErrors('Property name already existing!');
        }
		return redirect("/properties/{$newProperty->data->slug}");
    }

    /**
     * Will display a single property
     * 
     * @param  int $id 
     * @return Response
     */
    public function show($id)
    {
        $property = $this->showOneProperty($id);
        if(Auth::check())
        {
            $this->isPropertyAlreadyBooked($property->data);
        }
        return view('properties.show')
                ->with([
                    'property' => $property->data,
                    'propertyAlreadyBooked' => $this->propertyAlreadyBooked,
                    'new' => false
                ]); 
   }

    /**
     * Edit form for Property
     * 
     * @param  int $id 
     * @return Response
     */
    public function edit($id)
    {
        $property = $this->showOneProperty($id);
        return view('properties.edit')->with('property', $property->data);
    }

    /**
     * Will send updated fields to saamin api
     * 
     * @param  PropertyRequest $request
     * @param  int         $id      
     * @return Response
     */
    public function update(PropertyRequest $request, $slug)
    {
        $property = $this->updateProperty($request->all(), $slug);
        if(isset($property->error)){
            return back()->withErrors($property->error->message);
        }
        return redirect('/properties/'.$slug)->with('success', $property->data->message);
    }

    /**
     * Will send property to saamin api to be archived
     * 
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function destroy($id)
    {
        $property = $this->deleteProperty($id);
        return redirect('/properties')->with('success', $property->data->message);
    }
    /**
     * Will check if property is already booked by a customer
     * 
     * @param  Property $property 
     * @return boolean            
     */
    public function isPropertyAlreadyBooked($property)
    {
        $userProperties = $this->customerProperties(Auth::user()->id);
        foreach($userProperties->data as $userProperty)
        {
            if($userProperty->id == $property->id)
            {
                $this->propertyAlreadyBooked = true;
            }
        }
    }
}