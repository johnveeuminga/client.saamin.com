<?php

namespace App\Http\Middleware;

use Closure;

use Auth;

class CheckIfAgent
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(Auth::user()->userable_type != 'App\UserAgent'){
            return redirect('/');
        }

        return($next($request));

    }
}
