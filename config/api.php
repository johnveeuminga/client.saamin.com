<?php

return [

	'base_uri' => env('BASE_URI'),
	'api_key' => env('API_KEY'),
	'alg_env' => env('ALG_TABLE'),
	'app_id' => env('APP_ID')

];