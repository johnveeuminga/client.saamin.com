<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'passport' => [
        'client_secret' => env('CLIENT_SECRET'),
        'client_id' => env('CLIENT_ID'),
        'grant_type' => 'password'
    ],

    'facebook' => [
        'client_id' => '1713100939005521',
        'client_secret' => 'aa1d52fc95f6ee54358a19dfc2eda0b8',
        'redirect' => 'http://client.saamin.com/login/facebook/callback'

    ],

    'google' => [
        'client_id' => '802270074473-c8ss1o5km98kvvq9df0fah5kphetikh2.apps.googleusercontent.com',
        'client_secret' => 'dE8DTQxSWO7pGJGObLmvZ2Zl',
        'redirect' => 'http://client.saamin.com/login/google/callback'
    ]

];
